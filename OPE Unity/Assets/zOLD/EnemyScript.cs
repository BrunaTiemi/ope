﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyScript : MonoBehaviour
{
    public EnemyTypesInfo enemy;
    public GameObject parentObj;
    public MoveTowards moveTowards;
    public int vida, dano; //changed on Scriptable Object
    public float shotSpeed;
    public Rigidbody2D rb; //do objeto pai
    public TextMesh vidaText;
    public tipoAtq enemyType;
    public static event Action<int> damagePlayer; //manda evento para o player levar dano

    Vector3 shootDir;

    void Start(){
        vida = enemy.vida;
        dano = enemy.dano;
        vidaText.text = vida.ToString();
        //manda moveSpeed para script MoveTowards
        moveTowards.moveSpeed = enemy.moveSpeed;
    }

    void Attack(){
        //GameObject tempShot = Instantiate(enemy.shot, transform.position, Quaternion.identity);
        //Transform shotPos = tempShot.transform;
       // 
       ///Vector2 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
       //Vector2 newpos = Vector2.MoveTowards(shotPos.position, playerPos, shotSpeed*Time.deltaTime);
       //shotPos.position = newpos;
       
        //instancia o tiro
        GameObject tempShot = Instantiate(enemy.shot, transform.position, Quaternion.identity);
        //coloca a velocidade para ele ir pro lado
        Rigidbody2D rbShot = tempShot.GetComponent<Rigidbody2D>();

        if(moveTowards.facingLeft == true)
            rbShot.velocity = new Vector2(-shotSpeed, 0f);
            
        else if(moveTowards.facingLeft == false)
            rbShot.velocity = new Vector2(shotSpeed, 0f);
    }

    public void GetShootDir(Vector3 direction){
        shootDir = direction;
    }

    public void AttackNovo(){
        GameObject tiro = Instantiate(enemy.shot, transform.position, Quaternion.identity);
        Rigidbody2D rbShot = tiro.GetComponent<Rigidbody2D>();
        float moveSpeed = 800f;
        rbShot.AddForce(shootDir * moveSpeed, ForceMode2D.Impulse);

        //instancia o tiro
        //GameObject tempShot = Instantiate(enemy.shot, transform.position, Quaternion.identity);
        //coloca a velocidade para ele ir pro lado
        //Rigidbody2D rbShot = tempShot.GetComponent<Rigidbody2D>();

        //if(moveTowards.GetPlayerPos() != null){
        //    Vector2 playerPos = moveTowards.GetPlayerPos();
        //    shotPos.position = Vector2.MoveTowards(transform.position, playerPos, shotSpeed);
        //}

        //if(moveTowards.facingLeft == true)
        //    rbShot.velocity = new Vector2(-shotSpeed, 0f);
            
        //else if(moveTowards.facingLeft == false)
        //    rbShot.velocity = new Vector2(shotSpeed, 0f);
    }

    IEnumerator AttackDelay(){
        yield return new WaitForSeconds(1f);
        //if(moveTowards.playerReachable){//se o player está colidindo com a area do inimigo
        //    shootDir = moveTowards.GetShootAngle();
            Attack();
        //}
    }
    void OnTriggerEnter2D(Collider2D other){
        switch(other.gameObject.tag){
            case "BulletPlayer":    vida -= 2;
                                    Knockback();
            break;

            case "Player": damagePlayer(dano);
            break;

            case "Espada": 
                    vida--;
                    Knockback();
            break;

            default:
            break;
        }

        vidaText.text = vida.ToString();
        
        if(vida <= 0){
            Destroy(parentObj.gameObject);
        }
    }
    private void Knockback(){
        //knockback
        if(moveTowards.facingLeft)
            rb.velocity = Vector2.right * 18 + Vector2.up * 10;
                        
        else rb.velocity = Vector2.left * 18 + Vector2.up * 10;

    }
    private void OnBecameVisible() {
        InvokeRepeating("Attack", 0.5f, 2f);
    }
    private void OnBecameInvisible() {
        CancelInvoke("Attack");
    }

    [System.Serializable]
    public enum tipoAtq{ground, air, aquatic, shooty, strong}

}
