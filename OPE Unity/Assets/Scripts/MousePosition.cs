﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePosition : MonoBehaviour
{
    public Camera Camera;
    private void Update() {
        Camera.ScreenToWorldPoint(Input.mousePosition);
    }
}
