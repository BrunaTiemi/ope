﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalMoveTowards : MoveTowards
{
    protected void OnTriggerStay2D(Collider2D other) {        
        //se for o player
        if(other.gameObject.tag == "Player"){

            //vira para o lado certo e se move para lá
            if(other.transform.position.y < transform.position.y - offset.x){
                rb.velocity = Vector3.down * moveSpeed;
                //transform.position += Vector3.down * moveSpeed;
            }
            if(other.transform.position.y > transform.position.y + offset.y) {//direita
                //transform.position += Vector3.up * moveSpeed;
                rb.velocity = Vector3.right * moveSpeed;
            }
        }
    }
}
