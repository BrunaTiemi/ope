﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontFallOffPlatforms : MonoBehaviour
{
    public MoveTowards moveTowards;
    public LayerMask groundLayer, entityLayer;
    public BoxCollider2D bottomCol, frontCol;
    public BoxCollider2D groundCol;
    
    [Header("Colocar TODOS os colliders do objeto")]
    public Collider2D[] colliders;

    bool isVisible;

    private void Start() {
        //ignora colisoes com o mesmo objeto
        for (int i = 0; i < colliders.Length; i++)
        {
            for (int j = 0; j < colliders.Length; j++)
            {
                Physics2D.IgnoreCollision(colliders[i], colliders[j]);
            }
        }
    }

    void FixedUpdate(){
        if(isVisible){
            if(bottomCol.IsTouchingLayers(groundLayer)){
                moveTowards.OnLedge(false);
            }
            else 
                moveTowards.OnLedge(true);

            if(frontCol.IsTouchingLayers(groundLayer) || frontCol.IsTouchingLayers(entityLayer)){
                moveTowards.HasHitWall(true);
            }
            else 
                moveTowards.HasHitWall(false);

            if(groundCol.IsTouchingLayers(groundLayer)){
                moveTowards.IsFalling(false);
            }
            else    
                moveTowards.IsFalling(true);
        }
    }

    private void OnBecameVisible() {
        isVisible = true;
    }
    private void OnBecameInvisible() {
        isVisible = false;
    }
}
