﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour
{
    public Rigidbody2D rb;
    private groundEnemyRaycast groundEnmRaycast; // ativa o check do raycast quando vira o inimigo

    public bool facingLeft, paroudeandar, playerReachable, isOnLedge, wallAhead, isFalling;
    public bool isDying;
    public bool temVisaoPlayer;
    public float moveSpeed, minDistanceToPlayer; //changed on Scriptable Object
    public float movespeednormal;

    public Vector3 offset;

    //public LayerMask GroundCheck;

    //Animacoes
    public Animator anim;

    public bool enemyDoesNotMove;

    private int idleWalkDirRnd => Random.Range(1, 3); //esquerda ou direita
    private int idleMove; // 1 esquerda, 2 direita, 0 parado
    private bool isMovingIdle;

    public bool isVisible;

    void Start(){
        if(rb == null){
            rb = gameObject.GetComponentInChildren<Rigidbody2D>();
        }
        if(anim == null){
            anim = gameObject.GetComponentInChildren<Animator>();
        }
        groundEnmRaycast = GetComponentInChildren<groundEnemyRaycast>();
        
        moveSpeed = movespeednormal;

        StartCoroutine(IdleWalk(idleWalkDirRnd));
    }

    private void FixedUpdate() {
        if(isVisible){
            //idle move
            if(idleMove == 1){ //esquerda
                MoveLeft();
            }else if (idleMove == 2){ //direita
                MoveRight();
            }
            else if(anim != null) anim.SetBool("move", false);

            if(playerReachable){
                Movement();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        switch(other.gameObject.tag){
            case "Player":
                playerReachable = true;

                if(temVisaoPlayer){
                    StopAllCoroutines();
                    isMovingIdle = false;
                }
            break;

           // case "SpawnArea":
            //    isVisible = true;
            //break;
        }
    }
    
    private void OnTriggerExit2D(Collider2D other) { //para de atirar
        switch(other.gameObject.tag){
            case "Player":        
                playerReachable = false;
                if(anim != null) anim.SetBool("move", false);
                StartCoroutine(IdleWalk(idleWalkDirRnd));
            break;

            //case "SpawnArea":
            //    isVisible = false;
            //break;
        }
    }

    private void OnEnable() {
        isVisible = true;
    }
    private void OnDisable() {
        isVisible = false;
    }

    void Movement(){
        if(temVisaoPlayer){
            //vira para o lado certo e se move para lá
            if(PlayerMovement._transform.position.x < transform.position.x -minDistanceToPlayer){
                FollowPlayerLeft();
            }
            else if(PlayerMovement._transform.position.x > transform.position.x + minDistanceToPlayer) {//direita
                FollowPlayerRight();
            }   
        }
        //se nao conseguir enxergar o player, comeca o movimento idle de novo
        else if(isMovingIdle == false){
            isMovingIdle = true;
            StartCoroutine(IdleWalk(idleWalkDirRnd));
        }
    }

    void FollowPlayerLeft(){
        //para o movimento idle
        if(idleMove != 0){
            StopAllCoroutines();
            StopIdleMovement();
        }

        //vira e anda
        if(!facingLeft) Flip(true);

        MoveLeft();
    }

    void FollowPlayerRight(){             
        //para o movimento idle
        if(idleMove != 0){
            StopAllCoroutines();
            StopIdleMovement();
        }

        //vira e anda
        if(facingLeft) Flip(false);

        MoveRight();
    }

    void MoveLeft(){
        if(!enemyDoesNotMove)   {
            if(!isOnLedge && !isFalling && !wallAhead && !paroudeandar){
                //vira o sprite
                if(!facingLeft) Flip(true);

                if(anim != null) anim.SetBool("move", true);
                //transform.position += new Vector3( -moveSpeed *Time.deltaTime, 0);
                rb.velocity = Vector3.left * moveSpeed;
            }
        }
    }
    void MoveRight(){     
        if(!enemyDoesNotMove)  {
            if(!isOnLedge && !isFalling && !wallAhead && !paroudeandar){
                //vira o sprite
                if(facingLeft) Flip(false);

                if(anim != null) anim.SetBool("move", true);
                //transform.position += new Vector3( +moveSpeed *Time.deltaTime, 0);
                rb.velocity = Vector3.right * moveSpeed;
            }
        }
    }

    //usado nos scripts dos inimigos
    public Vector3 GetShootAngle(){
        Vector3 aimDir = (PlayerMovement._transform.position - transform.position).normalized;
        return aimDir;
    }

    public void ParaDeAndar(bool state){
        // para de andar
        if(state){
            moveSpeed = 0f;
            paroudeandar = true;
            if(anim != null) anim.SetBool("move", false);
        }
        else{ //volta a andar
                moveSpeed = movespeednormal;
                paroudeandar = false;
        }
    }
    public void ParaDeAndarSemMudarVelocidade(bool state){

        // para de andar
        if(state){
            paroudeandar = true;
            if(anim != null) anim.SetBool("move", false);
        }
        else{ //volta a andar
            paroudeandar = false;
        }
    }

    public void OnLedge(bool activate){
        isOnLedge = activate;

        if(isOnLedge == true && isMovingIdle){
            EdgeTurnAround();
        }
    }
    public void IsFalling(bool activate){
        isFalling = activate;
    }
    public void HasHitWall(bool activate){
        //se esta se movendo idle
        if(activate == true && isMovingIdle){
            EdgeTurnAround();
        }
        wallAhead = activate;
    }

    public void EdgeTurnAround(){
        if(facingLeft){
            Flip(false);
        }
        else Flip(true);

        //se estiver no movimento idle, vira automaticamente
        if(idleMove != 0){
            if(idleMove == 1) idleMove = 2;
            else idleMove = 1;
        }
    }

    void Flip(bool left){
        if(!isDying){ //nao pode virar quando estiver morrendo para nao flickar
            if(left){
                transform.Rotate(0f, 180f, 0f);
                facingLeft = true;
            }
            else
            {
                transform.Rotate(0f, 180f, 0f);
                facingLeft = false;
            }
            ParaDeAndar(false);

            if(groundEnmRaycast != null && canCheckRaycast) {
                print("checouuuu" + gameObject.name);
                groundEnmRaycast.VoidCheck();
                StartCoroutine(RaycastDelay());
            }
        }
    }

    bool canCheckRaycast = false;
    IEnumerator RaycastDelay(){ // pra nao gerar um loop infinito e congelar o app
        canCheckRaycast = false;

        yield return new WaitForSeconds(0.1f);

        canCheckRaycast = true;
    }

    #region Idle Movement

        void StopIdleMovement(){
            idleMove = 0;
            isMovingIdle = false;
        }

        protected virtual IEnumerator IdleWalk(int dir){
            isMovingIdle = true;
            
            //comeca a se mover pelo update para a direcao indicada
            idleMove = dir;
            //se quiser andar pra uma direcao que nao pode
            if(wallAhead) dir = dir == 1 ? 2 : 1;
            
            yield return new WaitForSeconds(Random.Range(0.6f, 1.5f));

            idleMove = 0;
            anim.SetBool("move", false);

            yield return new WaitForSeconds(0.5f);

            //continua andando idle
            StartCoroutine(IdleWalk(idleWalkDirRnd));

        }

    #endregion
}
