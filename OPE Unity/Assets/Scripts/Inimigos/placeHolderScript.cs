﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placeHolderScript : MonoBehaviour
{
    private GameObject enemy;
    public PrefabsSO enemyPrefabs;
    private nivelDiferentePrefab nivelAtualPrefabs;

    //public bool inimigoTemporario;
    public int indexNum;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "SpawnArea"){
            GetEnemyPrefab();
            GameObject inimigo = Instantiate(enemy, transform.position, Quaternion.identity);
            //if(inimigoTemporario) 
            //    inimigo.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
            Destroy(this.gameObject);
        }
    }

    void GetEnemyPrefab(){
        int nivel = GameManager.nivelAtual - 1;
        print("nivel" + nivel);
        nivelAtualPrefabs = enemyPrefabs.prefabsPorNivel[nivel];

        enemy = nivelAtualPrefabs.nivelPrefabs[indexNum];
    }
}
