﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class enemyShot : MonoBehaviour
{

    public Rigidbody2D rb;
    public float shotSpeed;
    bool wasParried;

    public MoveTowards moveTowards;

    public int damage = 1;

    public static event Action<int> hitPlayer;
    private void Start() {
       // print(transform.position);    

        //depois de tantos segundos, é destruido
        Invoke("SelfDestroy", 3f);
    }

    void OnTriggerEnter2D(Collider2D other){
        switch(other.gameObject.tag){
            case "Player":      PlayerScript pscript = other.GetComponent<PlayerScript>();
                                wasParried = pscript.parry;
                                    if(wasParried == false){
                                        hitPlayer(damage);
                                        Destroy(this.gameObject);
                                    }
                                else{ rb.velocity = new Vector2(-rb.velocity.x, -rb.velocity.y);
                                      this.gameObject.tag = "BulletPlayer";
                                }
                                //Debug.Log(wasParried);
            break;

            case "Espada": Destroy(this.gameObject);
            break;

            case "parry": wasParried = true;
            break;
            case "Tilemap": Destroy(this.gameObject);
            break;

            case "Enemy": //is was parried
                if(gameObject.tag == "BulletPlayer"){
                    Destroy(this.gameObject);
                }
            break;

            default: 
            break;
        }
    }

    void OnBecabeInvisible(){
       SelfDestroy();
    }

    void SelfDestroy(){
        Destroy(this.gameObject);
    }

    public void SetDamage(int dmg){
        damage = dmg;
    }
}
