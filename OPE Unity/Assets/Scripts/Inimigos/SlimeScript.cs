﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeScript : EnemyClass
{
    public CheckCollidingPlayer checkCollidingPlayer; //ve ser o player esta perto o suficiente pra atacar ele

    protected override void Start()
    {
        base.Start();
        moveTowards.ParaDeAndarSemMudarVelocidade(true);
    }

    public void PulindoAnda(){        
        moveTowards.ParaDeAndarSemMudarVelocidade(false);
    }
    public void PulinhoParaDeAndar(){
        moveTowards.ParaDeAndarSemMudarVelocidade(true);
    }


    //quando encosta no player toca a animacao de ataque
    //animacao da trigger a funcao de dar dano no player
    public void DamagePlayer(){    
        if(colidindoComPlayer){
            base.DamagePlayerEvent(dano, "animacao");
        }
    }

    protected override void OnTriggerExit2D(Collider2D other) {
        base.OnTriggerExit2D(other);
        if(other.gameObject.tag == "Player"){            
            anim.SetBool("Attack", false);
        }
    }
}
