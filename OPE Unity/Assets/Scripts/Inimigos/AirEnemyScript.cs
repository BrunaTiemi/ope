﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirEnemyScript : EnemyClass
{
    //
    //acessa _playerPosition
    //

    Vector3 shootDir;

    bool invoked = false;
    bool dead;


    public void GetShootDir(){
        Vector3 playerPosition = PlayerMovement._transform.position;
        shootDir = (playerPosition - transform.position).normalized;
    }

    void Attack(){
        if(!dead && temVisaoPlayer && !isFrozen){
            //instantiate and get rb
            GameObject tiro = Instantiate(enemy.shot, transform.position, Quaternion.identity);
            Rigidbody2D rbShot = tiro.GetComponent<Rigidbody2D>();

            //Add force
            float moveSpeed = 5f;
            GetShootDir();
            //print("shotSpeed = " + moveSpeed + " ShootDir = " + shootDir);
            rbShot.AddForce(shootDir * moveSpeed, ForceMode2D.Impulse);
        }
    }

    protected override void Update(){
        base.Update();

        if(moveTowards.playerReachable && !invoked){
            BeginAttack();
        } else if(invoked && !moveTowards.playerReachable){
            StopAttack();
        }
    }

    void BeginAttack(){
        InvokeRepeating("Attack", 0.5f, attackDelay);
        invoked = true;
    }

    void StopAttack(){
         CancelInvoke("Attack");
         invoked = false;
    }

    protected override void OnDeathStopAttack(){
        StopAttack();
        dead = true;
        //print("bbbbb");
    }

}
