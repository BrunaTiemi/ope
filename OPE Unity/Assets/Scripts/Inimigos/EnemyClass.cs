﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class EnemyClass : MonoBehaviour
{
    public EnemyTypesInfo enemy;
    public GameObject parentObj;
    public MoveTowards moveTowards;
    public SpriteRenderer spriteRenderer;

    public int vida, dano; //changed on Scriptable Object
    private int maxVida;
    public float moveSpeed, attackDelay;
    public Rigidbody2D rb; //do objeto pai


    [Header("Mostrar Vida")]
    private Slider sliderVida;
    private GameObject sliderParent; // obj pai do slider vida pra ser destruido junto
    public GameObject SliderVidaPrefab;
    public Vector3 distanciaVida = new Vector3(0f, 0.43f, 0f); //distancia da vida pro inimigo

    public static event Action<int> damagePlayer; //manda evento para o player levar dano
    public Animator anim;

    public Vector2 knockbackSpeed = new Vector2(24f, 12f);

    LayerMask groundLayerMask;
    public bool temVisaoPlayer;
    int frameCount = 0; //da update no raycast a cada 20 frames


    //Estados
    public bool isFrozen;
    public float frozenTime; //por quanto tempo vai ficar congelado
    float frozenUntil;
    private Color frozenColor;

    public bool colidindoComPlayer;

    [Header("Opcoes de dano")]
    public bool dontDamageInstantly;


    protected virtual void Start(){
        //pega os valores do Scriptable Object
        vida = enemy.vida;
        maxVida = vida;
        dano = enemy.dano;

        frozenColor = new Color32(136, 255, 255, 255);

        //manda moveSpeed para script MoveTowards
        moveTowards.movespeednormal = enemy.moveSpeed;
        moveTowards.moveSpeed = enemy.moveSpeed;

        if(moveTowards == null){
            moveTowards = transform.parent.gameObject.GetComponent<MoveTowards>();
        }
        if(rb == null){
            rb = gameObject.GetComponent<Rigidbody2D>();
        }
        if(anim == null){
            anim = gameObject.GetComponent<Animator>();
        }

        InstanciaSlider();

        //seta a layermask do chao
        groundLayerMask = LayerMask.GetMask("CanStandOn");
    }

    protected virtual void Update() {
        VidaSegueInimigo();
    }

    protected virtual void FixedUpdate(){
        if(moveTowards.playerReachable && frameCount > 60){
            Debug.DrawLine(transform.position, PlayerMovement._transform.position, Color.black);
            temVisaoPlayer = !Physics2D.Linecast(transform.position, PlayerMovement._transform.position, groundLayerMask);
            moveTowards.temVisaoPlayer = temVisaoPlayer;

            frameCount = 0;
        }

        frameCount++;
    }

    public void Knockback(){
        //disables walk
        moveTowards.ParaDeAndar(true);

        //knockback
        if(PlayerMovement._transform.position.x < transform.position.x)
            rb.velocity = Vector2.right * knockbackSpeed.x + Vector2.up * knockbackSpeed.y;
        
        else rb.velocity = Vector2.left * knockbackSpeed.x + Vector2.up * knockbackSpeed.y;

        anim.SetTrigger("takeDMG");

        //re-enables walk after 0.5s
        Invoke("PodeAndar", 0.5f);
    }
    void PodeAndar(){
        moveTowards.ParaDeAndar(false);
    }

    protected virtual void DamagePlayerEvent(int dano, string sender){
        damagePlayer(dano);
        print("dando dano mt cedo " + sender);
    }
    protected virtual void DamageAnimation(){
        anim.SetBool("Attack", true);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet") || other.gameObject.tag == "BulletPlayer") {
            LevaDano();
        }

        switch(other.gameObject.tag){
            case "Player": 
                if(!dontDamageInstantly) DamagePlayerEvent(dano, "tg enter"); //se da dano assim que encostar no player
                else DamageAnimation(); //se o inimigo usar um ataque pra dar dano
                
                colidindoComPlayer = true;            
            break;

            case "Espada": 
                    vida--;
                    Knockback();
            break;

            default:
            break;
        }
        
        AtualizaVida();
    } 
    
    //da dano no player enquanto estao encostando
    protected virtual void OnTriggerStay2D(Collider2D other){
        switch(other.gameObject.tag){

            case "Player":
                if(!dontDamageInstantly) DamagePlayerEvent(dano, "td stay"); //se da dano assim que encostar no player
                else DamageAnimation();
            break;

            case "Freezer": Freeze();
            break;

            default:
            break;
        }
    }
    protected virtual void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){            
            colidindoComPlayer = false;
        }
    }


    protected virtual void LevaDano(){
        vida --;
        Knockback();
        
        if(vida <= 0){
            Die();
        }
        
        //começa a atacar o jogador
        moveTowards.playerReachable = true;
    }

    public void Freeze(){
        //seta o tempo para parar de ficar congelado. Se a funcao for ativada de novo, vai so ficar mais tempo congelado.
        frozenUntil = Time.time + frozenTime;
        print("feezze");

        //comeca a corotina
        if(!isFrozen){
            StartCoroutine(FreezeCoroutine());
        }
    }

    IEnumerator FreezeCoroutine(){
        if(frozenTime == 0) frozenTime = 2f;
        isFrozen = true;
        moveTowards.enabled = false;
        spriteRenderer.color = frozenColor;
        anim.enabled = false;

        yield return new WaitUntil(() => Time.time > frozenUntil);

        isFrozen = false;
        moveTowards.enabled = true;
        spriteRenderer.color = Color.white;
        anim.enabled = true;
    }

    protected virtual void AtualizaVida(){
        if(sliderVida != null) sliderVida.value = vida;
    }

    protected virtual void InstanciaSlider(){
        Transform canvas = GameObject.Find("CanvasTransicao").transform;

        sliderParent = Instantiate(SliderVidaPrefab, canvas);
        sliderVida = sliderParent.GetComponent<Slider>();

        sliderVida.maxValue = maxVida;
    }

    protected virtual void VidaSegueInimigo(){
        if(sliderVida != null) sliderVida.transform.position = new Vector3 (transform.position.x + distanciaVida.x, transform.position.y + distanciaVida.y, transform.position.z + distanciaVida.z);
    }

    
        public BoxCollider2D[] colliders1;
        public CompositeCollider2D[] colliders2;
        public CapsuleCollider2D[] colliders3;

    public void Die(){
        anim.enabled = true;
        moveTowards.isDying = true;
        anim.Play("deathAnim");

        //Disables colliders
        foreach (var c in colliders1){
            c.enabled = false;
        }
        foreach (var c in colliders2){
            c.enabled = false;
        }
        foreach (var c in colliders3){
            c.enabled = false;
        }

        //freezes enemy in position
        rb.constraints = RigidbodyConstraints2D.FreezeAll;

        //para de atacar 
        OnDeathStopAttack();
        Destroy(sliderParent);

        GameManager.inimigosMortos += 1;
    }
    private void OnDestroy() {
        if(sliderParent != null){
            Destroy(sliderParent);
        }
    }
    public void SelfDestroy(){
        Destroy(parentObj.gameObject);

        if(sliderVida != null) Destroy(sliderParent); //foi instanciado no canvas entao deve ser destruido tambem
    }

    protected virtual void OnDeathStopAttack(){

    }

}
