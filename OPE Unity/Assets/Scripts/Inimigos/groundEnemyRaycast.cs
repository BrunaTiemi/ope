﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class groundEnemyRaycast : MonoBehaviour
{
    MoveTowards moveTowards;

    //chao
    public float groundDistance = 0.6f, enemyLength = 0.8f, groundCheckOffset = -0.58f;
    Vector3 groundOffsetLeft;
    
    //void
    public float voidDistance = 2.78f; // void eh para nao vir do mapa
    public Vector3 voidCheckOffset = new Vector3(0.50f, 0f, 0f);
    Vector3 voidCheckOffsetLeft;

    //frente
    [Header("O azul nao pode encostar nele mesmo")]
    public Vector3 frontCheckOffset;
    Vector3 frontOffsetLeft = new Vector3(0.52f, 0f, 0f);
    public float frontCheckDistance = 0.05f;
    public LayerMask frontLayerMask; // no que ele nao pode empurrar ou vai bater
    

    bool isFacingLeft => moveTowards.facingLeft;

    private void Start() {
        moveTowards = GetComponentInChildren<MoveTowards>();

        voidCheckOffsetLeft = new Vector3( -voidCheckOffset.x, voidCheckOffset.y, voidCheckOffset.z);
        
        frontOffsetLeft = new Vector3 (-frontCheckOffset.x, frontCheckOffset.y, frontCheckOffset.z);

        InvokeRepeating("check", 0f, 0.2f);
    }

    public void check() {
        if(moveTowards.isVisible){
            GroundCheck();
            VoidCheck();
            EntityCheck();
        }
    }

    public void GroundCheck(){
        //linha reta pra baixo
        //Debug.DrawRay(transform.position, transform.TransformDirection(Vector2.down) * groundDistance, Color.red);
        //RaycastHit2D ground = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.down), groundDistance, LayerMask.GetMask("CanStandOn"));

        //esquerda direita
        float currentEnemyLength;
        Vector2 direction;
        if(isFacingLeft){
            currentEnemyLength = -enemyLength;
            direction = Vector2.left;
        } else {
            currentEnemyLength = enemyLength;
            direction = Vector2.right;
        }

        //raycast
        Debug.DrawRay(transform.position + new Vector3(-currentEnemyLength/2, groundCheckOffset, 0f), transform.TransformDirection(direction) * currentEnemyLength, Color.red);
        RaycastHit2D ground = Physics2D.Raycast(transform.position + new Vector3(-currentEnemyLength/2, groundCheckOffset, 0f), transform.TransformDirection(direction), currentEnemyLength, LayerMask.GetMask("CanStandOn"));
    
        moveTowards.IsFalling(!ground);
        // if(!ground) print("caindo ");
    }

    public void VoidCheck(){
        Vector3 currentOffset;
        //diferencia entre esquerda e direita
        if(isFacingLeft){
            currentOffset = voidCheckOffsetLeft;
        } else {
            currentOffset = voidCheckOffset;
        }

        //raycast
        Debug.DrawRay(transform.position + currentOffset, transform.TransformDirection(Vector2.down) * voidDistance, Color.red);
        RaycastHit2D temChaoNaFrente = Physics2D.Raycast(transform.position + currentOffset, transform.TransformDirection(Vector2.down), voidDistance, LayerMask.GetMask("CanStandOn"));

        moveTowards.OnLedge(!temChaoNaFrente);
    }

    public void EntityCheck(){ //para nao empurrar outros mobs
        Vector2 curDir;
        Vector3 offset;

        if(isFacingLeft){
            curDir = Vector2.left;
            offset = frontOffsetLeft;
        } else {
            curDir = Vector2.right;
            offset = frontCheckOffset;
        }

        Debug.DrawRay(transform.position + offset, transform.TransformDirection(curDir) * frontCheckDistance, Color.blue);
        RaycastHit2D bateuEmAlgo = Physics2D.Raycast(transform.position + offset, transform.TransformDirection(curDir), frontCheckDistance, frontLayerMask);

        if(bateuEmAlgo){
            moveTowards.HasHitWall(true);
        }
        else moveTowards.HasHitWall(false);

    }

#if UNITY_EDITOR
    private void Update() {
    }
#endif
}
