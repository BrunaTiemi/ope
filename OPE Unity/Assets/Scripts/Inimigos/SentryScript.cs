﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SentryScript : MonoBehaviour
{
    public static event Action<int> DamagePlayer;
     //Ativar e desativar de acordo com o Spawner
    public bool isActive, turnedRight;

    public Transform minRotationTransform, maxRotationTransform;
    public Transform aimObj;
    public float aimMoveSpeed, switchTime;
    private Quaternion nextRot;

    public float shootDelay;
    private float shotTime;

    Quaternion aimRotation => aimObj.rotation;
    Quaternion maxRotation => maxRotationTransform.rotation;
    Quaternion minRotation => minRotationTransform.rotation;

    bool currentMin;
    bool currentMax => !currentMin;

    //FOUND PLAYER
    public BoxCollider2D aimCollider;
    public CheckCollidingPlayer checkCollidingAim;
    [Tooltip("Verifica se o player esta dentro da area")]
    public CheckCollidingPlayer checkColArea;
    
    public bool foundTarget => checkCollidingAim.colliding;
    public bool followTarget;
    private bool targetInsideArea => checkColArea.colliding;
    private bool targetLeftArea => checkColArea.colliding == false;
    Vector3 aimDir => (PlayerMovement._transform.position - transform.position).normalized;
    private float aimToPlayerAngle => Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;

    private void OnTriggerEnter2D(Collider2D other) { //Ativa as funcionalidades do bloco quando o player entrar na area.
        if(other.gameObject.tag == "SpawnArea"){
            isActive = true;
            nextRot = maxRotation;
            currentMin = true;
            if(turnedRight){
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }

            StartCoroutine(AimPath());
        }
        else if(other.gameObject.tag == "DeactivateArea"){
            isActive = false;
            StopAllCoroutines();
        }
    }

    #region movement
    private void FixedUpdate() {
        //move se nao achou o player
        if(isActive && !foundTarget && !followTarget){
            Move();
        }//se achou o player, move em direcao a ele
        else if((foundTarget && targetInsideArea) || followTarget){
            AimTowardsPlayer();
            followTarget = true;
        }
        //se player saiu da area, para de seguir.
        if(targetLeftArea){
            followTarget = false;
        }
    }

    private void Move(){
        aimObj.rotation = Quaternion.Lerp(aimRotation, nextRot , aimMoveSpeed * Time.fixedDeltaTime);
        print($"moving {aimMoveSpeed * Time.fixedDeltaTime}");
    }

    private void AimTowardsPlayer(){
        aimObj.eulerAngles = new Vector3(0f, 0f, aimToPlayerAngle);
        shotTime += Time.deltaTime;

        if(shotTime > shootDelay){
            Attack();
        }
    }

    protected virtual void Attack(){
        DamagePlayer(1);
        shotTime = 0;
    }

    IEnumerator AimPath(){ //a mira vai e volta
        SwitchRotTimer();

        yield return new WaitForSeconds(switchTime);

        SwitchRotTimer();

        yield return new WaitForSeconds(switchTime);

        if(isActive){
            StartCoroutine(AimPath());
        }
        
    }

    private void SwitchRotTimer(){
        //troca a posicao de uma pra outra
        if(currentMin){
            currentMin = false;
            nextRot = maxRotation;
        }
        else{
            currentMin = true;
            nextRot = minRotation;
        }
    }

    #endregion
}
