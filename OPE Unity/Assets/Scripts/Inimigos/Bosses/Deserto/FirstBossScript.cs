﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class FirstBossScript : MonoBehaviour
{
    public EnemyTypesInfo enemy;
    public GameObject parentObj;
    public int vida, dano; //changed on Scriptable Object
    public float shotSpeed;

    public SpriteRenderer cabecaSpriteRenderer;

    //Attack
    public float ShootattackDelay;
    int currentAttack; //0 = nao atacando, 1 = atacando
    Vector3 shootDir;
    public Transform shootPos, shootTowards;[SerializeField]
    bool canAttack;
    
    //public static event Action<int> damagePlayer; //manda evento para o player levar dano
    
    public Animator anim;

    //Damage
    public Sprite[] damageSprites;
    public SpriteRenderer[] bossSpriteRenderer;
    public Slider healthSlider;

    //DeathAnim
    public Animator[] bolasAnim;
    int numDestruct = 5; //vai somando para destruir em ordem

    bool playerToTheLeft => transform.position.x > PlayerMovement._transform.position.x;

    void Start(){
        //pega os valores do Scriptable Object
        vida = enemy.vida;
        healthSlider.maxValue = vida;        
        healthSlider.value = vida;
        dano = enemy.dano;
        numDestruct = 5;
        canAttack = true;
    }

    void OnEnable(){
        PlayerScript.PlayerRespawn += Start;
    }
    void OnDisable(){
        PlayerScript.PlayerRespawn -= Start;
    }

    private void FixedUpdate() {
        if(playerToTheLeft){
            cabecaSpriteRenderer.flipX = false;
        }
        else cabecaSpriteRenderer.flipX = true;
    }

    void ShootAttack(){
        //instantiate and get rb
        GameObject tiro = Instantiate(enemy.shot, shootPos.position, Quaternion.identity);
        Rigidbody2D rbShot = tiro.GetComponent<Rigidbody2D>();

        //seta o dano do tiro
        enemyShot tiroScript = tiro.GetComponent<enemyShot>();
        tiroScript.SetDamage(enemy.dano);

        //Add force
        float shotSpeed = 10f;
        GetShootDir();
        rbShot.AddForce(shootDir * shotSpeed, ForceMode2D.Impulse);
    }
    public void GetShootDir(){
        Vector3 playerPosition = PlayerMovement._transform.position;
        shootDir = (shootTowards.position - shootPos.position).normalized;
    }

    void FanAttack(){
        InvokeRepeating("ShootAttack", 0.01f, ShootattackDelay);

        if(playerToTheLeft)
            anim.Play("FanAttack");
        else anim.Play("FanAttackRight");
    }

    void RayAttack(){
        if(playerToTheLeft)
            anim.Play("RayAttackLeft");
        else anim.Play("RayAttackRight");
    }

    void StopAttack(){
         CancelInvoke("ShootAttack");
    }

    public void SwitchAttacks(){
        if(canAttack){
            int lastAttack = currentAttack;
            int newAttack = UnityEngine.Random.Range(0, 3);

            //nao deixa que seja o mesmo ataque duas vezes
            while (newAttack == lastAttack){
                newAttack = UnityEngine.Random.Range(0, 3);
            }
            currentAttack = newAttack;

            Attack();
        }
        print(currentAttack);
    }

    public void Attack(){
        StopAttack();
        if(currentAttack == 1){
                FanAttack();
        }
        else if(currentAttack == 2){
            RayAttack();
        }
        else {
            anim.Play("Idle");
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "BulletPlayer" || other.gameObject.tag == "Espada"){
            if(canAttack)
                TakeDamage();
            print("dmg");
        }
    }

    void TakeDamage(){
        for(int i = 1; i < bolasAnim.Length; i++){
            bolasAnim[i].enabled = false;
        }

        bossSpriteRenderer[0].sprite = damageSprites[2];
        for(int i = 1; i < bossSpriteRenderer.Length; i++){
            bossSpriteRenderer[i].sprite = damageSprites[3];
        }

        carabranca.SetActive(true);

        vida--;

        Invoke("SpritesBackToNormal", 0.2f);

        healthSlider.value = vida;

        if(vida < 1){
            Die();
        }
    }

    public GameObject carabranca;
    void SpritesBackToNormal(){
        bossSpriteRenderer[0].sprite = damageSprites[0];
        for(int i = 1; i < bossSpriteRenderer.Length; i++){
            bossSpriteRenderer[i].sprite = damageSprites[1];
        }
        
        for(int i = 1; i < bolasAnim.Length; i++){
            bolasAnim[i].enabled = false;
        }
        carabranca.SetActive(false);
    }


    void Die(){
        CancelInvoke();
        canAttack = false;
        anim.Play("FanAttack");
        SpritesBackToNormal();
        InvokeRepeating("DeathAnimation", 0f, 0.4f);

        
        for(int i = 1; i < bolasAnim.Length; i++){
            bolasAnim[i].enabled = true;
        }

        Invoke("NextLevel", 4f);
    }

    void DeathAnimation(){

        if(numDestruct >= 0){
            bolasAnim[numDestruct].Play("death");
            numDestruct--;
        }
        else CancelInvoke();
            
        this.enabled = false;
    }
}
