﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationEvents : MonoBehaviour
{
    public FirstBossScript firstBossScript;    
    public GameObject nextLevelObj;

    public void FirstBossSwitchAttack(){
        firstBossScript.SwitchAttacks();
    }
    
    public void NextLevel(){
        nextLevelObj.SetActive(true);
    }
}
