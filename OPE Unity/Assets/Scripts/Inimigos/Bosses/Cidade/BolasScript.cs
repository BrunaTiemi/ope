﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BolasScript : MonoBehaviour
{
    public static event Action OnBallTakeDamage;

    public int vida = 15;
    public GameObject whiteDamage;
    public GameObject shot;
    public CityBossScript cityBoss;

    //tiro
    public float shotSpeed;
    
    //pega posicoes do player e seta a direcao da mira
    private Vector3 targetPos => PlayerMovement._transform.position;
    private Vector3 aimDir => (targetPos - transform.position).normalized;

    public bool turnedRight;

    public Transform minRotationTransform, maxRotationTransform;
    public Transform aimObj;
    public float aimMoveSpeed;

    public bool delay; //delay entre um tiro e outro
    public float shootDelay;
    private float shotTime; //tempo ate atirar, vai somando com o tempo.

    Quaternion aimRotation => aimObj.rotation;
    Quaternion maxRotation => maxRotationTransform.rotation;
    Quaternion minRotation => minRotationTransform.rotation;

    //FOUND PLAYER
    public BoxCollider2D aimCollider;
    public CheckCollidingPlayer checkCollidingAim;
    [Tooltip("Verifica se o player esta dentro da area")]
    public CheckCollidingPlayer checkColArea;
    
    public bool foundTarget => checkCollidingAim.colliding;
    public bool followTarget;
    private bool targetInsideArea => checkColArea.colliding;
    private bool targetLeftArea => checkColArea.colliding == false;
    private float aimToPlayerAngle => Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;
    
    public bool _foundTarget;
    public bool _targetInsideArea;

    public bool dead;
    private void Start() {
        cityBoss = GameObject.FindObjectOfType<CityBossScript>();
        shot = cityBoss.enemy.shot;
   }

   public void FixedUpdate() {
        if(((followTarget || foundTarget) && targetInsideArea)){
            //AimTowardsPlayer();
            followTarget = true;
        }
        //se player saiu da area, para de seguir.
        if(targetLeftArea){
            followTarget = false;
        }

        _foundTarget = foundTarget;
        _targetInsideArea = targetInsideArea;
   }
   public void AimTowardsPlayer(){
        aimObj.eulerAngles = new Vector3(0f, 0f, aimToPlayerAngle);
        shotTime += Time.deltaTime;

        if(!delay){
            Attack();
            StartCoroutine(ShootDelay());
        }
    }

    void OnTriggerEnter2D(Collider2D other){
       switch(other.gameObject.tag){
            case "BulletPlayer":
            case "Espada":
                vida--;
                whiteDamage.SetActive(true);
                if(OnBallTakeDamage != null){
                    OnBallTakeDamage();
                }

                if(vida < 1){
                    dead = true;
                    gameObject.SetActive(false);
                }
                break;
       }
    }

    private void Attack(){
        //Instancia o tiro e seta a direcao certa
        Rigidbody2D tiroRb = Instantiate(shot, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
        tiroRb.AddForce(aimDir * shotSpeed, ForceMode2D.Impulse);
    }

    IEnumerator ShootDelay(){
        delay = true;

        yield return new WaitForSeconds(shootDelay);

        delay = false;
    }
}
