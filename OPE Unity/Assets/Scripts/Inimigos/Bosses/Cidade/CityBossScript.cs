﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityBossScript : MonoBehaviour
{
    public EnemyTypesInfo enemy;
    public int vida;
    public Transform[] movePositions;
    private Transform playerTransform = PlayerMovement._transform;
    public Animator anim;
    public GameObject WhiteDamage;
    public Slider healthSlider;

    public float speed = 1;
    private Vector3 nextPosition;
    private int currentPosNum = 1; //index da posicao atual
    private Vector3 lerpPos;

    //Bolas
    public List<BolasScript> bola;
    private int bolasVida => bola[0].vida + bola[1].vida;
    private int sliderVida => vida + bolasVida;    
    private GameObject ballShot;

    //double ray ATK
    public Transform[] rays;
    public Transform[] raysTargetRotation;
    public float doubleRayMoveSpeed = 1;
    public float doubleRayDelay = 0.5f;
    float doubleRayAttackFrom;
    private bool handsFollowingPlayer => bola[0].followTarget == true || bola[1].followTarget == true;
    public Transform lastAtkTarget;
    
    public Transform[] initialRaysTransform;

    [Range(0, 3)]
    public int currentATK; // 0 = moving, 1 = ataque direto pra baixo, 2 = dois raios pros lados que vao pro meio, 3 = mira que locka no player
    public bool isMoving => (currentATK == 0);
    bool downRayATK => (currentATK == 1);
    bool doubleRayATK => (currentATK == 2);
    bool doubleRayCanAnimate => Time.time > doubleRayAttackFrom;

    string ANIM_DOWNRAYATK = "DownRayATK";
    string currentAnim;

    public bool testando;

    [Header("Tempo ")]
    public float downRayTime;
    public float doubleRayTime;

    //Mudar as tags pro tiro parar de atravessar o boss
    bool bossTagHasChanged;
    private bool handsDead => bola[0].dead && bola[1].dead;

    bool invokedWalk; // para nao var invoke dnv

    private void Start() {
        vida = enemy.vida;
        healthSlider.maxValue = sliderVida;
        healthSlider.value = sliderVida;
        ballShot = enemy.shot;

        nextPosition = transform.position; //para ele nao teleportar pra longe no inicio
        if(!testando){
            if(!invokedWalk) {
                invokedWalk = true;
                InvokeRepeating("PickNextPos", 3f, 3f);
            }

            Invoke("PickNextAttack", 2f);
        }
        
        StopAllCoroutines();
    }

    private void OnEnable(){
        PlayerScript.PlayerRespawn += Start;
        BolasScript.OnBallTakeDamage += UpdateSlider;
    }
    private void OnDisable(){
        PlayerScript.PlayerRespawn -= Start;
        BolasScript.OnBallTakeDamage -= UpdateSlider;
    }

    IEnumerator WaitUntilAtkEnds(float time){
        yield return new WaitForSeconds(time);

        PickNextAttack();
    }

    private void Update() {
        if(!dying){
            if(isMoving){
                Move();
            }
            else if(downRayATK){
                DownRayATK();
            }
            else if(doubleRayATK){
                DoubleRayATK();
            }

            if(testando){
                if(Input.GetKeyDown(KeyCode.B)){
                    PickNextAttack();
                }
            }
        }
    }

    void PickNextAttack(){
        int rnd;

        do{
            rnd = Random.Range(0, 3);
        } while (rnd == currentATK);

        currentATK = rnd;

        //reseta os valores
        
        currentAnim = "none";
        anim.Play("None");
        
        for(int i = 0; i < rays.Length; i++){            
            rays[i].gameObject.SetActive(false);
            bola[i].followTarget = false;
            rays[i].rotation = initialRaysTransform[i].rotation;
        }

        //invoke para mudar de ataque depois
        if(downRayATK) {
            StartCoroutine(WaitUntilAtkEnds(downRayTime));
        }
        
        else if(doubleRayATK){
            doubleRayAttackFrom = doubleRayDelay + Time.time;
            StartCoroutine(WaitUntilAtkEnds(doubleRayTime));
        }
        else{//se estiver movendo
            StartCoroutine(WaitUntilAtkEnds(1f));
        }
    }

    void PickNextPos(){

        //se o player estiver a direita do boss
        if(PlayerMovement._transform.position.x > transform.position.x){
            if(currentPosNum == 0){
                currentPosNum = 1; //vai p centro
            }
            else currentPosNum = 2; //vai p direita
        }
        else{ //se esta a esquerda
            if(currentPosNum == 2){
                currentPosNum = 1; //vai p centro
            }
            else currentPosNum = 0;//vai p esquerda
        }

        nextPosition = movePositions[currentPosNum].position;
    }

    void Move(){
        lerpPos = Vector3.Lerp(transform.position, nextPosition, speed * Time.fixedDeltaTime);
        transform.position = lerpPos;
    }

    void DownRayATK(){
        if(currentAnim != ANIM_DOWNRAYATK){
            anim.Play(ANIM_DOWNRAYATK);
            currentAnim = ANIM_DOWNRAYATK;
        }
    }

    void DoubleRayATK(){
        for(int i = 0; i < rays.Length; i++){
            rays[i].gameObject.SetActive(true);
        }

        if(doubleRayCanAnimate && !handsFollowingPlayer){
            rays[0].rotation = Quaternion.Lerp(rays[0].rotation, raysTargetRotation[0].rotation, doubleRayMoveSpeed * Time.fixedDeltaTime);
            rays[1].rotation = Quaternion.Lerp(rays[1].rotation, raysTargetRotation[1].rotation, doubleRayMoveSpeed * Time.fixedDeltaTime);
        }
        else if(handsFollowingPlayer){
            if(bola[0].isActiveAndEnabled) bola[0].AimTowardsPlayer();
            if(bola[1].isActiveAndEnabled) bola[1].AimTowardsPlayer();
        }

        if(handsDead){
            SecondDoubleRay();
        }
    }

    bool delay;
    public float lastAtkDelayTime = 0.3f;
    void SecondDoubleRay(){
        if(!delay){
        Rigidbody2D tiroRb = Instantiate(ballShot, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
        Vector3 aimDir = (lastAtkTarget.position - transform.position).normalized;
        tiroRb.AddForce(aimDir * 4, ForceMode2D.Impulse);
        StartCoroutine(ShootDelay());
        }
    }
    IEnumerator ShootDelay(){
        delay = true;

        yield return new WaitForSeconds(lastAtkDelayTime);

        delay = false;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "BulletPlayer" || other.gameObject.tag == "Espada"){
            if(handsDead){
                //muda a tag do boss pra Enemy para os tiros pararem de atravessar ele.
                if(!bossTagHasChanged){
                    gameObject.tag = "Enemy";
                    bossTagHasChanged = true;
                }
                TakeDamage();
            }
            UpdateSlider();
        }
    }

    public GameObject fimDoJogoText;
    bool dying;
    void TakeDamage(){        
        vida--;
        WhiteDamage.SetActive(true);

        if(vida < 1 && !dying){
            Die();
        }
    }
    void Die(){
        dying = true;

        CancelInvoke();
        StopAllCoroutines();

        anim.SetTrigger("Dying");
    }
    public void SelfDestroy(){        //animator
        EndStage();
        Destroy(transform.parent.gameObject);
    }

    void UpdateSlider(){
        healthSlider.value = sliderVida;
    }

    void EndStage(){
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.EndStageWithDelay(1.5f);
    }
}
