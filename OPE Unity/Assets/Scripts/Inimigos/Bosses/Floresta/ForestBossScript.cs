﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ForestBossScript : MonoBehaviour
{
    public static event Action OnBossDied;
    public Camera mainCamera;
    private CameraShake cameraShake;

    public GameObject bossParentObj;
    
    public EnemyTypesInfo enemy;
    public int vida, maxVida;
    public Animator anim, moveAnim;
    public Slider healthSlider;
    public SpriteRenderer sr;
    
    public float moveSpeed = 1;

    public bool isFrozen, isDead;
    public float frozenTime; //por quanto tempo vai ficar congelado
    float frozenUntil;
    public Color frozenColor;

    [Header ("Hands") ]
    private SpriteRenderer[] handsSR;
    public ForestBossHands[] hands;
    bool bothHandsNormalPos => hands[0].inNormalPos && hands[1].inNormalPos;

    public SpawnFirePillars firePillars;
    
#if(UNITY_EDITOR)
    private void OnEnable() {
        KeyboardEvents.OnPressedF += hands[0].PunchPlayer;

        KeyboardEvents.OnPressedG += hands[0].PunchGround;
        KeyboardEvents.OnPressedG += hands[1].PunchGround;
    }
    private void OnDisable() {        
        KeyboardEvents.OnPressedF -= hands[0].PunchPlayer;

        KeyboardEvents.OnPressedG += hands[0].PunchGround;
        KeyboardEvents.OnPressedG -= hands[1].PunchGround;
    }
#endif

    private void Start() {
        GetReferences();

        maxVida = enemy.vida;
        vida = maxVida;

        healthSlider.maxValue = maxVida;
        healthSlider.value = vida;

        PickAttack();
    }
    void GetReferences(){        
        if(mainCamera == null) mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>(); 
        cameraShake = mainCamera.GetComponent<CameraShake>();

        handsSR = new SpriteRenderer[hands.Length];
        for (int i = 0; i < hands.Length; i++)
        {
            handsSR[i] = hands[i].GetComponent<SpriteRenderer>();
        }
    }

    void PunchPlayer(int hand){ // 0 esq 1 dir

        if(hand == 0) hands[0].PunchPlayer();
        else hands[1].PunchPlayer();
    }
    void PunchGround(){
        hands[0].PunchGround();
        hands[1].PunchGround();
    }

    public void ScreenShake(){
        cameraShake.ShakeCamera();
        firePillars.SpawnPillarRandom();
    }
    public void LessShakyScreenShake(){
        cameraShake.LessShakyShake();
    }

    List<int> lastAttacks = new List<int>();
    void PickAttack(){
        int atk = UnityEngine.Random.Range(0,5);
        int tries = 0;

        if(lastAttacks.Count > 2)
        while(lastAttacks[0] == lastAttacks[1] && lastAttacks[0] == atk && tries > 150){
            atk = UnityEngine.Random.Range(0,5);
            tries ++;
        }

        lastAttacks.Add(atk);
        if(lastAttacks.Count > 3){
            lastAttacks.RemoveAt(0);
        }


        if(atk == 4){
            PunchGround();
        } else {
            PunchPlayer(atk);
        }

        StartCoroutine(atkCoroutine());
        /*switch (atk)
        {
            case 0:             
            case 1: PunchPlayer(atk);
            break;
            
            case 4: PunchGround();
            break;
            
        }*/
    }

    IEnumerator atkCoroutine(){
        yield return new WaitUntil(() => bothHandsNormalPos);

        if(!isDead)
            PickAttack();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        switch(other.gameObject.tag){
            case "BulletPlayer":
            case "Espada":
                LevaDano();
            break;

            case "Freezer":
                Freeze();
            break;

        }
    }

    public void LevaDano(){
        if(isFrozen){
            vida -= 6;
        }
        else vida--;

        AtualizaVida();

        if(vida < 1){
            Die();
        } else {
            anim.SetTrigger("TakeDamage");
            hands[0].DamageAnimation();
            hands[1].DamageAnimation();
        }
    }
    protected virtual void AtualizaVida(){
        healthSlider.value = vida;
    }
    
    void Die(){
        Debug.Log("MORREU");

        for (int i = 0; i < hands.Length; i++)
        {
            hands[i].gameObject.SetActive(false);
        }
        moveAnim.enabled = false;

        isDead = true;

        anim.SetTrigger("Die");
    }

    void SelfDestroy(){
        OnBossDied?.Invoke();
        EndStage();        
        Destroy(bossParentObj);
    }
    void EndStage(){
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.EndStageWithDelay(1.5f);
    }

    public void Freeze(){
        //seta o tempo para parar de ficar congelado. Se a funcao for ativada de novo, vai so ficar mais tempo congelado.
        frozenUntil = Time.time + frozenTime;

        //comeca a corotina
        if(!isFrozen){
            StartCoroutine(FreezeCoroutine());
        }
    }

    IEnumerator FreezeCoroutine(){
        isFrozen = true;
        sr.color = frozenColor;
        ChangeHandsColor(frozenColor);
        moveAnim.enabled = false;
        EnableBothHands(false);

        yield return new WaitUntil(() => Time.time > frozenUntil);

        isFrozen = false;
        sr.color = Color.white;
        ChangeHandsColor(Color.white);
        if(!isDead) {
            moveAnim.enabled = true;
            EnableBothHands(true);
        }
    }
    void ChangeHandsColor(Color c){
        for (int i = 0; i < handsSR.Length; i++)
        {
            handsSR[i].color = c;
        }
    }
    void EnableBothHands(bool enable){
        for (int i = 0; i < hands.Length; i++)
        {
            hands[i].enabled = enable;
        }
    }
}
