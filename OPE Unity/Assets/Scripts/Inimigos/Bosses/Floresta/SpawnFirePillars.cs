﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFirePillars : MonoBehaviour
{
    public Transform[] posicoes;

    public GameObject pillarPrefab;

    bool isSpawning;

    public float time;


    //posicoes aleatorias pra spawnarem
    int one;
    int two;
    int three;

    int randomPos => Random.Range(0, posicoes.Length);

    private void OnEnable() {
        ForestBossHands.OnFinishedPunching += CanSpawn;
    }private void OnDisable() {
        ForestBossHands.OnFinishedPunching -= CanSpawn;
    }

    public void SpawnPillarRandom(){
        if(!isSpawning){
            StartCoroutine(SpawnCoroutine());
        }
    }

    IEnumerator SpawnCoroutine(){        
        isSpawning = true;
        GetPos();
        InvokeRepeating("Spawn", 0f, 0.1f);

        yield return new WaitForSeconds(time);

        CancelInvoke();
    }

    void CanSpawn(){        
        isSpawning = false;
    }

    void GetPos(){        
        int tries = 0;

        //para nao pegar posicoes iguais
        do {            
            one = randomPos;
            two = randomPos;
            three = randomPos;
            tries++;
        } while( (one == two  || two == three || one == three) && tries < 200);

    }
    void Spawn(){
        Instantiate(pillarPrefab, posicoes[one]);
        Instantiate(pillarPrefab, posicoes[two]);
        Instantiate(pillarPrefab, posicoes[three]);
    }
}
