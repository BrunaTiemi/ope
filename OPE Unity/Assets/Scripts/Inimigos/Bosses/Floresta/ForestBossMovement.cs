﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestBossMovement : MonoBehaviour
{
    public Transform xMax, xMin;
    public float maxMoveDelay, minMoveDelay;

    public Transform[] movePosicoes;
    int posIndex = 0;

    float randomTime =>  Random.Range(minMoveDelay, maxMoveDelay);

    Vector3 playerPos =>  PlayerMovement._transform.position;

    private Vector3 lerpPos, startPos;
    private Vector3 nextPosition;
    public float moveSpeed = 5;

    

    public bool canMove = true;


    private void Start() {
        startPos = transform.position;
        StartCoroutine(GetPosCoroutine());
    }

    private void FixedUpdate() {
        Move();
    }

    void Move(){        
        lerpPos = Vector3.Lerp(transform.position, nextPosition, moveSpeed * Time.fixedDeltaTime);
        transform.position = lerpPos;
    }

    void GetNextPos(){
        float dist = 0f; //distancia para ver qual esta mais perto do player

        dist = Vector2.Distance(transform.position, playerPos);

        //paga a posicao mais perta das pre definidas
        for (int i = 0; i < movePosicoes.Length-1; i++)
        {
            float newDistance = Vector2.Distance(movePosicoes[i].position, movePosicoes[i+1].position);
            if(newDistance < dist){
                dist = newDistance;
                posIndex = i; //seta a pos mais perto do player
            }
        }

        nextPosition = new Vector3(movePosicoes[posIndex].position.x, startPos.y, startPos.z);
        print("next pos");
    }

    IEnumerator GetPosCoroutine(){
        yield return new WaitForSeconds(randomTime);

        
        GetNextPos();
        

        if(canMove){
            StartCoroutine(GetPosCoroutine());
        }
    }
}
