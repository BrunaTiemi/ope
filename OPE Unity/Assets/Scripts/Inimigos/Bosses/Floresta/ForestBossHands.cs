﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ForestBossHands : MonoBehaviour
{
    public static event Action OnFinishedPunching;
    public ForestBossScript forestBossScript;
    private Animator anim;
    public Transform normalPosTransform, handsObjT;

    public Transform groundPos; // pra nao passar do chao na batidona

    Vector3 lerpPos;
    Vector3 punchStartPos, punchEndPos;
    Vector3 preparationBackPoint; // antes do soco, vai pra tras, para essa posicao aqui
    public bool preparingPunch, punchingPlayer, inNormalPos, goingToNormalPos; //inNormalpos - posicao inicial, do lado do boss 
    public bool aimPlayer, backToNormalRotation; // se mirar no player ou nao
    public bool moveBack;
    public float punchSpeed = 7;
    public Vector3 velocity = Vector3.zero;

    //socao no chao
    public bool punchingGround;

    Quaternion inicialRotation;

    float delayAfterPunching = 1.1f; // quanto tempo a mao vai ficar parada no chao depois de bater

    //sprites
    [Tooltip("0 = normal, 1 = preparando, 2 = soco")]
    public Sprite[] handSprites;
    SpriteRenderer spriteRenderer;

    //check para ir pra posicao normal mesmo quando n tiver ido
    float timeToBeBackToNormal;

    private void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();

        //desativa o animator pra animar pelo codigo
        anim = GetComponent<Animator>();
        anim.enabled = false;

        //posicoes iniciais
        inicialRotation = Quaternion.identity;
        inNormalPos = true;
    }

    private void Update() {
        if((punchingPlayer || punchingGround) && !goingToNormalPos){
            PunchingPlayer();
            print("PunchingPlayerndo");
        }
        else if(goingToNormalPos){
            BackToNormalPosition();
        }
        if(aimPlayer) AimTowardsPlayer(true);
        else if(backToNormalRotation) AimTowardsPlayer(false);

        if(moveBack) MoveBack();
    }

    public void DamageAnimation(){
        if(inNormalPos){
            anim.enabled = true;
            anim.SetTrigger("TakeDamage");
        }
    }

    void IsInNormalPos(){ // o que acontece ao chegar pra posicao normal
        goingToNormalPos = false;
        inNormalPos = true;            
        backToNormalRotation = false;
        
        ChangeTag(Tags.DontDamagePlayer);

        //evento que acabou de bater
        Invoke("FinishedPunchingEvent", 0.2f); //delay pra n bugar
    }

    void FinishedPunchingEvent(){        
        OnFinishedPunching?.Invoke();
    }

    #region Socao
    public void PunchPlayer(){
        if(inNormalPos){ //so pode socar se estiver na posicao normal
            //faz a preparacao e soca
            StartCoroutine(PreparacaoProSocao());
        }
    }

    void PunchingPlayer(){        
        lerpPos = Vector3.SmoothDamp(transform.position, punchEndPos, ref velocity, punchSpeed * Time.fixedDeltaTime);
        transform.position = lerpPos;

        inNormalPos = false;

        //acabou a movimentacao
        if(Vector2.Distance(transform.position, punchEndPos) < 0.2f){
            aimPlayer = false;
            StartCoroutine(BackNormalPosDelay());

            //se esta dando o socao no chao, screen shaka
            if(punchingGround){
                forestBossScript.ScreenShake();
            } else {
                forestBossScript.LessShakyScreenShake();
            }
        } 
    }

    void BackToNormalPosition(){
        lerpPos = Vector3.SmoothDamp(transform.position, normalPosTransform.position, ref velocity, 20 * Time.fixedDeltaTime);
        transform.position = lerpPos;
        
        //chegou na posicao normal
        if(Vector2.Distance(transform.position, normalPosTransform.position) < 0.05f ||
            Time.time > timeToBeBackToNormal){
            IsInNormalPos();
        }
        ChangeSprite(0);
    }

    IEnumerator BackNormalPosDelay(){
        yield return new WaitForSeconds(delayAfterPunching);

        //volta para a posicao inicial
        punchingPlayer = false;
        punchingGround = false;
        goingToNormalPos = true;
        transform.SetParent(normalPosTransform); //muda o pai pra ficar mexendo com o boss

        timeToBeBackToNormal = Time.time + 3f; // marca o time em que eh pra ja estar na posicao certa, pra n ficar preso depois
        
        //volta para a rotacao inicial
        yield return new WaitForSeconds(0.2f);
        backToNormalRotation = true;
    }

    #endregion
    #region Preparacao pro Socao

    void AimTowardsPlayer(bool aimTowardsPlayer){
        //eh diferente do aimPlayer pq quando aquele esta falso ele continua na mesma rotacao, nao volta a inicial.
        if(aimTowardsPlayer){ // mira em direcao ao player.
            Vector3 vectorToTarget = PlayerMovement._transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg+90;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 5);
        } else { //volta para a rotacao inicial    
            transform.rotation = Quaternion.Slerp(transform.rotation, inicialRotation, Time.deltaTime * 5);
        }

    }

    IEnumerator PreparacaoProSocao(){
        //comeca a mirar no player e vai pra tras
        inNormalPos = false;
        backToNormalRotation = false;
        aimPlayer = true;
        moveBack = true;

        yield return new WaitForSeconds(0.5f);

        ChangeSprite(2);

        yield return new WaitForSeconds(0.5f);

        //para de ir pra tras
        moveBack = false;

        yield return new WaitForSeconds(0.3f);

        //pega as posicoes do player
        punchStartPos = transform.position;
        punchEndPos = PlayerMovement._transform.position;     

        yield return new WaitForSeconds(0.2f);

        //para de mirar no player e soca
        aimPlayer = false;
        punchingPlayer = true;
        transform.SetParent(handsObjT); //muda o pai pra n ficar mexendo errado
        ChangeTag(Tags.DamagePlayer);
    }
    void MoveBack(){
        transform.position = transform.position + transform.up * 0.03f;
    }

    #endregion

    public void ChangeSprite(int num){ // 0 = normal, 1 = preparando, 2 = soco
        anim.enabled = false;
        spriteRenderer.sprite = handSprites[num];
    }

    #region Batidona no chao

    public void PunchGround(){
        if(inNormalPos)
            StartCoroutine(PreparacaoPunchGround());
    }

    IEnumerator PreparacaoPunchGround(){
        //vai pra tras
        moveBack = true;
        inNormalPos = false;
        
        yield return new WaitForSeconds(0.7f);
        
        ChangeSprite(1);

        yield return new WaitForSeconds(0.3f);
        
        //para de ir pra tras
        moveBack = false;

        yield return new WaitForSeconds(0.5f);
        
        ChangeSprite(2);

        //pega o Y do chao pra bater
        punchStartPos = transform.position;
        punchEndPos = new Vector3(transform.position.x, groundPos.position.y, transform.position.z);

        //batidona
        punchingGround = true;
        transform.SetParent(handsObjT); //muda o pai pra n ficar mexendo errado
        ChangeTag(Tags.DamagePlayer);
    }

    #endregion

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet")){
                forestBossScript.LevaDano();
        }
    }

    void ChangeTag(Tags tag){
        //troca para a tag escolhida
        //tag boss nao da dano no player, enemy da dano.
        switch (tag)
        {
            case Tags.DamagePlayer: gameObject.tag = "DamagePlayer";
            break;
            
            case Tags.DontDamagePlayer: gameObject.tag = "Boss";
            break;
        }
    }

    enum Tags{
        //tag boss nao da dano no player, enemy da dano.
        DontDamagePlayer, DamagePlayer
    }
}
