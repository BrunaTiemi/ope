﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAttackScript : EnemyClass
{
    Vector3 shootDir;

    [SerializeField]bool invoked = false;
    bool dead;

    public void GetShootDir(){
        Vector3 playerPosition = PlayerMovement._transform.position;
        shootDir = (playerPosition - transform.position).normalized;
    }

    void Attack(){
        if(!dead){
            //instantiate and get rb
            GameObject tiro = Instantiate(enemy.shot, transform.position, Quaternion.identity);
            Rigidbody2D rbShot = tiro.GetComponent<Rigidbody2D>();

            //Add force
            float moveSpeed = 5f;
            GetShootDir();
            //print("shotSpeed = " + moveSpeed + " ShootDir = " + shootDir);
            rbShot.AddForce(shootDir * moveSpeed, ForceMode2D.Impulse);
        }
    }

    protected override void Update(){
        base.Update();
        if(moveTowards.playerReachable && !invoked){
            AttackWithDelay();
        } else if(invoked && !moveTowards.playerReachable){
            StopAttack();
        }
    }

    void BeginAttack(){print("attakck");
        InvokeRepeating("Attack", 0.5f, attackDelay);
        invoked = true;
    }

    void StopAttack(){
         CancelInvoke();
         invoked = false;
    }

    protected override void OnDeathStopAttack(){
        StopAttack();
        dead = true;
        //print("bbbbb");
    }

    void AttackWithDelay(){
        InvokeRepeating("attackDelayToInvoke", 0.5f, 4f);
    }

    void attackDelayToInvoke(){
        StartCoroutine(attacksDelay());
    }

    IEnumerator attacksDelay(){
        //tempo entre os ataques
        InvokeRepeating("Attack", 0f, attackDelay);

        yield return new WaitForSeconds(0.3f);

        StopAttack();

    }

}
