﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterText : MonoBehaviour
{
    public Text text;
    public List<LetterTextText> textQueue = new List<LetterTextText>();

    private void OnEnable() {
        KeyboardEvents.OnPressedF += NextText;    
    }
    private void OnDisable() {
        KeyboardEvents.OnPressedF -= NextText;    
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            NextText();
        }
    }

    public void NextText(){
        //se tiver texto, toca ele na tela.
        if(textQueue.Count > 0){
            //coloca o texto na tela
            text.text = textQueue[0].conteudo;
            //remove o texto do queue
            textQueue.RemoveAt(0);

            //ativa o objeto de texto depois de mudar o texto
            gameObject.SetActive(true);
        }//se nao tiver, desabilita o objeto de texto
        else {
            gameObject.SetActive(false);
        }
    }

    public void AddTextToQueue(string titulo, string conteudo){
        textQueue.Add(new LetterTextText( titulo, conteudo ));
    }

    public void AddToQueueAndPlay(string titulo, string conteudo){
        AddTextToQueue(titulo, conteudo);
        NextText();
    }
    
}
public class LetterTextText{
    public string titulo;
    public string conteudo;

    public LetterTextText(string title, string content){
        titulo = title;
        conteudo = content;
    }
}
