﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationTransitions : MonoBehaviour
{
    public GameObject[] stage; //ordem de ativacao dos objetos
    public StageBotoes[] stageBotoes;

    public Button[] voltarButtons;

    int stageNum; //current stage

    public struct StageBotoes{
        public Transform[] botoes;
    }

    private void OnEnable() {
        if(voltarButtons != null)
        //inscreve os botoes para o GoBackStage
        for(int i = 0; i < voltarButtons.Length; i++){
            voltarButtons[i].onClick.AddListener(GoBackStage);
        }
    }
    private void OnDisable() {
        stageNum = 0;
        for (int i = 1; i < stage.Length; i++){
            stage[i].SetActive(false);
        }
        stage[0].SetActive(true);
    }
    public void AdvanceStage(){
        if(stageNum + 1 < stage.Length){
            stage[stageNum].SetActive(false);
            stageNum++;
            stage[stageNum].SetActive(true);
        }
    }

    public void GoBackStage(){
        if(stageNum-1 >= 0 ){
            stage[stageNum].SetActive(false);
            stageNum--;
            stage[stageNum].SetActive(true);
        }
    }

    public void BackToStageOne(){
        foreach(GameObject obj in stage){
            obj.SetActive(false);
        }

        stageNum = 0;
        stage[stageNum].SetActive(true);
    }
}
