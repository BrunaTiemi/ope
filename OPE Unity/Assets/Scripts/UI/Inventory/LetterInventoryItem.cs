﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterInventoryItem : InventoryItem
{
    public CartasSO cartasSO;

    [TextArea]
    public string conteudo;

    public GameObject TextPrefab;

    public void SendValues(int index, InventoryMenu invScript){
        id = index;
        invMenu = invScript;
        icon.sprite = cartasSO.cartas[index].sprite;
        nameText.text = cartasSO.cartas[index].titulo;
        conteudo = cartasSO.cartas[index].conteudo;


        AddOnClickListener();
    }
    public override void PlayText(){
        invMenu.ShowTextOnScreen(cartasSO.cartas[id].conteudo);
    }
}
