﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{   
    public int id; //pra mostrar o texto certo

    [HideInInspector]
    public InventoryItemSO itemSO;
    protected InventoryMenu invMenu;

    [SerializeField]
    protected Image icon;
    [SerializeField]
    protected Text nameText;

    public virtual void SendValues(InventoryItemSO so, int index, InventoryMenu InvScript){
        id = index;
        itemSO = so;
        invMenu = InvScript;

        AddOnClickListener();
        LoadVisual();
    }

    void LoadVisual(){
        icon.sprite = itemSO.icon;
        nameText.text = itemSO.nome;
    }

    protected virtual void AddOnClickListener(){
        GetComponent<Button>().onClick.AddListener(PlayText);
    }

    public virtual void PlayText(){
        print("mostra texto");
        invMenu.ShowTextOnScreen(itemSO.conteudo);
    }
}
