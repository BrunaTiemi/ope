﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "InventoryItemSO", menuName = "OPE/UI/InventoryItemSO")]
public class InventoryItemSO : ScriptableObject
{
    public Sprite icon;
    public Ability ability;

    [SerializeField]
    string nomePT, nomeEN;
    [SerializeField] [TextArea]
    string conteudoPT, conteudoEN;

    public string nome{
        get{
            Language language = LanguageManager.currentLanguage;
            switch (language)
            {
                case Language.Portuguese: 
                    return nomePT;

                default: 
                    return nomeEN;
            }
        }
    }
    
    public string conteudo{
        get{
            Language language = LanguageManager.currentLanguage;
            switch (language)
            {
                case Language.Portuguese: 
                    return conteudoPT;

                default: 
                    return conteudoEN;
            }
        }
    }
}
