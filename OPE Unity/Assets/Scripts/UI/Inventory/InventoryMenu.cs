﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InventoryMenu : MonoBehaviour
{
    public GameObject inventoryObj;
    private SaveGame save;

    public GameObject inventoryItemPrefab, letterInvItemPrefab;


    [Tooltip("Botoes usados para abrir as abas")]
    public GameObject[] tabButtons;

    [Tooltip("Objetos que serao ativados ao clicar nos botoes das abas")]
    public GameObject[] tabMenus;
    const int abilityTabNum = 0;
    const int letterTabNum = 1;

    public bool isOpen => inventoryObj.activeSelf;

    public InventoryItemSO[] habilidadesSO;
    public CartasSO cartasSO;
    public LetterText canvasText;

    bool canOpen;

    //eventos
    private void OnEnable() {
        KeyboardEvents.OnPressedI += ToggleInventory;
        GetAbilityTile.EnableAbility += AddAbility;
        CartaScript.OnCartaLeu += AddLetter;
    }
    private void OnDisable() {
        KeyboardEvents.OnPressedI -= ToggleInventory;
        GetAbilityTile.EnableAbility -= AddAbility;
        CartaScript.OnCartaLeu -= AddLetter;
    }


    ///<summary>
    //// carrega os itens do save e manda pros scripts dos itens
    ///</summary>

    private void Start() {
        save = GameObject.FindObjectOfType<SaveGame>();

        Invoke("LoadAbilities", 0.1f);
        Invoke("LoadLetters", 0.1f);
    }

    #region Carrega Itens
    void LoadAbilities(){
        if(save.gameData.hasParry){
            InstantiateItem(0, abilityTabNum);
        }
        if(save.gameData.hasWaterRay){
            InstantiateItem(1, abilityTabNum);
        }
        if(save.gameData.hasTripleBalls){
            InstantiateItem(2, abilityTabNum);
        }
        if(save.gameData.hasFollowingBall){
            InstantiateItem(3, abilityTabNum);
        }
        if(save.gameData.hasMousePosAtk){
            InstantiateItem(4, abilityTabNum);
        }
        if(save.gameData.hasFreeze){
            InstantiateItem(5, abilityTabNum);
        }
    }
    void AddAbility(Ability abil){
        for (int i = 0; i < habilidadesSO.Length; i++)
        {   
            //instancia com o numero do SO certo
            if(abil == habilidadesSO[i].ability){
                InstantiateItem(i, abilityTabNum);
                return;
            }
        }
    }

    void LoadLetters(){
        //se n tem o bastante no save, inicializa com o tanto do SO
        if(save.lettersData.cartas.Count < cartasSO.cartas.Length){
            save.lettersData.InicializaCartas(cartasSO.cartas.Length);
        }

        for (int i = 0; i < cartasSO.cartas.Length; i++)
        {
            //se tem a carta, adiciona
            if(save.lettersData.cartas[i] == true){
                InstantiateLetter(i, letterTabNum);
            }
        }
    }
    void AddLetter(int num){
        InstantiateLetter(num, letterTabNum);
    }

    void InstantiateItem(int itemIndex, int menuIndex){
        GameObject go = Instantiate(inventoryItemPrefab, tabMenus[menuIndex].transform);
        go.GetComponent<InventoryItem>().SendValues(habilidadesSO[itemIndex], itemIndex, this);
    }
    void InstantiateLetter(int itemIndex, int menuIndex){
        GameObject go = Instantiate(letterInvItemPrefab, tabMenus[menuIndex].transform);
        go.GetComponent<LetterInventoryItem>().SendValues(itemIndex, this);
    }
    #endregion

    public void ShowTextOnScreen(string text){
        canvasText.AddToQueueAndPlay("", text);
    }


    public void OpenTab(int tabNum){
        //desativa td
        for (int i = 0; i < tabMenus.Length; i++)
        {
            tabMenus[i].SetActive(false);
        }
        //ativa o certo
        if(tabMenus.Length > tabNum) tabMenus[tabNum].SetActive(true);
    }

    #region Abre e Fecha
    public void ToggleInventory(){
        //fecha
        if(isOpen){{
            CloseInventory();
            }
        } //abre
        else {
            OpenInventory();
        }
    }
    void OpenInventory(){
        if(!canOpen){
            //procura o objeto que msotra a cena pra n ficar preso la caso a pessoa tente abrir o inventario cedo
            if(GameObject.FindObjectOfType<ShowStageScreen>() != null) return;
            else canOpen = true;        
        }


        //pausa e abre
        inventoryObj.SetActive(true);
        Time.timeScale = 0;
    }
    public void CloseInventory(){      
        //despausa e fecha o inventario  
        inventoryObj.SetActive(false);            
        Time.timeScale = 1;
    }

    #endregion

}
