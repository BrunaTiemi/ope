﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class ShowStageScreen : MonoBehaviour
{
    public TextosSO textosSO;
    int nivel;
    int fase;

    public TextMeshProUGUI nivelText, faseText;

    private void Start() {
        nivel = GameManager.nivelAtual;
        fase = GameManager.faseAtual;

        nivelText.text = GetNivel(nivel);


        string fasePrefix = textosSO.fase[Array.FindIndex(textosSO.fase, x => x.language == LanguageManager.currentLanguage)].text;

        if(fase == 4){
            faseText.text = "Boss Fight";
        } else {            
            faseText.text = $"{fasePrefix} {fase}";
        }
    }

    string GetNivel(int lv){
        lv--;
        return textosSO.niveis[lv].linguas[Array.FindIndex(textosSO.niveis[lv].linguas, x => x.language == LanguageManager.currentLanguage)].text;
    }
}
