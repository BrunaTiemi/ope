﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FitSpriteToRect : MonoBehaviour
{
    RectTransform rectTransform;
    SpriteRenderer sr;
    ContentSizeFitter sizeFitter;
    TextMeshPro tmpro;

    private void Start() {
        rectTransform = GetComponent<RectTransform>();
        sr = GetComponent<SpriteRenderer>();
        sizeFitter = GetComponent<ContentSizeFitter>();
        tmpro = GetComponentInChildren<TextMeshPro>();
        
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);

        sr.size = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
    }
}
