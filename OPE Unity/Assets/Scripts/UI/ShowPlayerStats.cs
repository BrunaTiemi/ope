﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPlayerStats : MonoBehaviour
{
    public Image[] heartSprites, manaSprites;

    int mana, currentMana;
    int health, currentHealth;
                //oq ta mostrando na tela

    public Sprite fullHeart, emptyHeart;
    public Sprite fullMana, emptyMana;

    private PlayerScript playerScript;
    private PlayerHabilidades playerHabilidades;

    private void Start() {
        playerScript = GameObject.FindObjectOfType<PlayerScript>();
        playerHabilidades = playerScript.GetComponentInChildren<PlayerHabilidades>();
    }
    private void Update() {
        AtualizaVida();
        AtualizaMana();
    }
    public void AtualizaVida(){
        CalculaVida((int)playerScript.vida);
    }
    void CalculaVida(int vida){
        //spriteIndex = vida - (maxLife - healthPerSprite);
        int percentage = vida * 100 / playerScript.maxLife;

        //para nao descer muito rapido
        percentage += 10;

        //print(percentage.ToString() + " " + vida + " "  + maxLife.ToString());
        
        int tempHealth = 0;
        if(percentage > 0){
            tempHealth = percentage/22;
        }
        else tempHealth = 0;

        //nao pode passar de 5
        health = Mathf.Clamp(tempHealth, 0, 5);

        //print("health" + health);

        AtivaVida(health);
    }

    void AtivaVida(int num){
        //se ganhou vida
        if(num > currentHealth){
            for (int i = 0; i < num; i++)
            {
                heartSprites[i].sprite = fullHeart;
            }
        } else { //se perdeu vida            
            for (int i = 0; i < heartSprites.Length; i++)
            {
                heartSprites[i].sprite = emptyHeart;
            }
            for (int i = 0; i < num; i++)
            {
                heartSprites[i].sprite = fullHeart;
            }
        }


        currentHealth = num;
    }

    
    public void AtualizaMana(){
        CalculaMana((int)playerHabilidades.mana);
    }
    void CalculaMana(int newMana){
        //spriteIndex = vida - (maxLife - healthPerSprite);
        int percentage = newMana * 100 / (int)playerHabilidades.maxMana;

        //para nao descer muito rapido
        percentage += 10;

        //print(percentage.ToString() + " " + vida + " "  + maxLife.ToString());
        
        int tempMana = 0;
        if(percentage > 0){
            tempMana = percentage/22;
        }
        else tempMana = 0;

        //nao pode passar de 100
        mana = Mathf.Clamp(tempMana, 0, 10);

        //print("mana" + mana);

        AtivaMana(mana);
    }

    
    void AtivaMana(int num){
        //se ganhou vida
        if(num > currentMana){
            for (int i = 0; i < num; i++)
            {
                manaSprites[i].sprite = fullMana;
            }
        } else { //se perdeu vida            
            for (int i = 0; i < manaSprites.Length; i++)
            {
                manaSprites[i].sprite = emptyMana;
            }
            for (int i = 0; i < num; i++)
            {
                manaSprites[i].sprite = fullMana;
            }
        }


        currentMana = num;
    }
}
