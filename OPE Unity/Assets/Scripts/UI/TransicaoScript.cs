﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class TransicaoScript : MonoBehaviour
{
    public static event Action OnTransitionFinish;
    
    public void ChangeScene(){
        OnTransitionFinish();
        Debug.Log("Muda de cena");
    }

    public void FadeIn(){
        gameObject.SetActive(true);
        gameObject.GetComponent<Animator>().SetBool("fadeIn", true);
    }

    
    /*
    AsyncOperation carregarCena;

        StartCoroutine(LoadAsyncOperation());


    IEnumerator LoadAsyncOperation(string cena){ //operacao assincrona para carregar a proxima cena
        TransicaoScript.OnTransitionFinish += ChangeSceneNow;
        carregarCena = SceneManager.LoadSceneAsync(cena);
        carregarCena.allowSceneActivation = false;

        transicaoScript.FadeIn();

        yield return new WaitForSeconds(4.6f);
        carregarCena.allowSceneActivation = true;
    }

    void ChangeSceneNow(){
        carregarCena.allowSceneActivation = true;
    }
    */
}
