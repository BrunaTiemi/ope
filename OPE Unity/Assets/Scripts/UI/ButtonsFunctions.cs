﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsFunctions : MonoBehaviour
{
    public void RestartLevel(){
        try{ GameObject.FindObjectOfType<SaveGame>().RestartCheckpoint();}
        catch{ }

        SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex);
    }

    public void GoToScene(string cena){
        SceneManager.LoadScene(cena);
    }

    public void QuitGame(){
        Application.Quit();
        Debug.Log("Saiu do jogo");
    }

    public void SetInactive(GameObject obj){
        obj.SetActive(false);
    }

    public void Activate(GameObject obj){
        obj.SetActive(true);
    }
}
