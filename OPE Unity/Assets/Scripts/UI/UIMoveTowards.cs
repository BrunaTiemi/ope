﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMoveTowards : MonoBehaviour
{
    public Transform targetPos;
    Vector3 lerpPos;
    public float speed = 2;

    void Update(){        
        lerpPos = Vector3.Lerp(transform.position, targetPos.position, speed * Time.fixedDeltaTime);
        transform.position = lerpPos;
    }
}
