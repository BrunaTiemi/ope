﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesHideAbilities : MonoBehaviour
{
    private SaveGame save;
    public GameObject[] textos;

    private void OnEnable() {
        save = GameObject.FindObjectOfType<SaveGame>();
        if(save.IsLoaded())
        LoadAbilities();
    }
    void LoadAbilities(){
            if(save.gameData.hasParry){
                EnableText(2);
            } else DisableText(2);
            if(save.gameData.hasWaterRay){
                EnableText(3);
            } else DisableText(3);
            /*
            if(save.gameData.hasTripleBalls){
                EnableText(2);
            }
            if(save.gameData.hasFollowingBall){
                EnableText(3);
            }
            if(save.gameData.hasMousePosAtk){
                EnableText(4);
            }*/
            if(save.gameData.hasFreeze){
                EnableText(4);
            } else DisableText(4);
        }

    void EnableText(int text){
        if(textos.Length > text){
            textos[text].SetActive(true);
        }
    }
    void DisableText(int text){
        if(textos.Length > text){
            textos[text].SetActive(false);
        }
    }
}
