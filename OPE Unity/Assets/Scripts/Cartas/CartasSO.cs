﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "CartasSO", menuName = "OPE/CartasSO")]
public class CartasSO : ScriptableObject {
    public GameObject textPrefab;
    //index eh o id da carta.
    public CartasClass[] cartas;

    public void SetTitulos(){
        for (int i = 0; i < cartas.Length; i++)
        {
            cartas[i].tituloBR = "Carta " + (i+1);
            cartas[i].tituloEng = "Letter " + (i+1);            
        }
    }

    public void ResetarSaveCartas(){
        SaveGame save = GameObject.FindObjectOfType<SaveGame>();
        save.lettersData.SalvarCartasDoZero();
    }

    [System.Serializable]
    public class CartasClass{
        public string tituloBR, tituloEng;

        [TextArea]
        public string conteudoBR, conteudoEng;
        public Sprite sprite;

        public string titulo {
            get{
                switch (LanguageManager.currentLanguage)
                {
                    case Language.Portuguese: return tituloBR;

                    default: return tituloEng;
                }
            }
        }
        public string conteudo {
            get{
                switch (LanguageManager.currentLanguage)
                {
                    case Language.Portuguese: return conteudoBR;

                    default: return conteudoEng;
                }
            }
        }
    }
}