﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CartaScript : MonoBehaviour
{
    public static event Action<int> OnCartaLeu;
    public int id;
    public CartasSO cartaSO;
    public GameObject wasdInstrucao; // para o texto de cima
    private TranslatableTextMeshPro cimaTexto;

    bool dentroDaArea, mostrou, canClick = true;
    float cooldown = 0.6f;

    LetterText textBox;
    Inventory inv;

    public void MostrarEscrita(){
        mostrou = true;

        textBox = GameObject.FindObjectOfType<GameManager>().letterText;
        textBox.AddToQueueAndPlay("", cartaSO.cartas[id].conteudo);
    }

    private void OnBecameVisible() {
        canClick = true;
        KeyboardEvents.OnPressedF += ApertouF;
    }
    private void OnBecameInvisible() {
        KeyboardEvents.OnPressedF -= ApertouF;
    }

    void ApertouF(){//se o inv apretou f
        //se esta perto da carta
        if(dentroDaArea && canClick){
            //desliga a instrucao
            wasdInstrucao.SetActive(false);

            //se nao mostrou o texto, mostra
            if(!mostrou){
                MostrarEscrita();
                AddLetter(); //adiciona a carta no inventario
            }
            else{ //depois do inv ter lido a carta, eh destruida.
                Destroy(this.gameObject);
            }
        }
    }

    void AddLetter(){
        inv.AddLetter(id);
        OnCartaLeu?.Invoke(id);
    }

    IEnumerator ClickCooldown(){
        canClick = false;

        yield return new WaitForSeconds(cooldown);

        canClick = true;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player") {
            dentroDaArea = true;
            if(inv == null) inv = other.GetComponent<Inventory>();

            //ativa o texto
            wasdInstrucao.SetActive(true);
            PegaTextos();
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == "Player") {
            dentroDaArea = false;
            wasdInstrucao.SetActive(false);
        }
    }

    void PegaTextos(){
        if(cimaTexto == null) cimaTexto = GetComponentInChildren<TranslatableTextMeshPro>();
        
        string english = "Press 'F' to interact";
        string portuguese = "Aperte 'F' para pegar";
        
        cimaTexto.SetText(english, portuguese);
    }
}
