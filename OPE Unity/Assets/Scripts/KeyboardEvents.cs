﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KeyboardEvents : MonoBehaviour
{
    public static event Action OnPressedEsc;
    public static event Action OnPressedF, OnPressedG;
    public static event Action OnPressedI;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            OnPressedEsc?.Invoke();
            print("pressed ESC");
        }

        if(Input.GetKeyDown(KeyCode.F)){
            OnPressedF?.Invoke();
            print("pressed F");
        }

        if(Input.GetKeyDown(KeyCode.G)){
            OnPressedG?.Invoke();
            print("pressed G");
        }

        if(Input.GetKeyDown(KeyCode.I)){
            OnPressedI?.Invoke();
            print("pressed I");
        }

    }
}
