﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DamagePlayer : MonoBehaviour
{
    public static event Action<int> dmgPlayer;
    public int damage;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            dmgPlayer(damage);
        }
    }
}
