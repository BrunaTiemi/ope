﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CeilingCheck : MonoBehaviour
{
    public PlayerMovement player;

    void Start(){
        if(player == null){
            player = gameObject.GetComponentInParent<PlayerMovement>();
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Tilemap"){
            player.TouchedCeiling(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.tag == "Tilemap"){
            player.TouchedCeiling(false);
        }
    }
}
