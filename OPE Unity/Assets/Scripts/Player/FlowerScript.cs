﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerScript : MonoBehaviour
{
    public SpritesFlorInfo sprites;
    public SpriteRenderer spriteRenderer;
    public PlayerScript player;

    [SerializeField] int difPraMudarSprite = 10;

    int maxLife, spriteIndex;

    
    //follow player
    public Vector3 followDifference;
    private Vector3 playerPos, smoothPos;
    int setPositive = 1;

    private bool IsFar => (Vector3.Distance(PlayerMovement._transform.position, transform.position) > -followDifference.x); //used to prevent flower from moving when the x distance is too small
    private float speedMultiplier => player.movement.inputHorz * 6;
    private float inputHorz => player.movement.inputHorz;
    private Vector3 _FollowDifference => new Vector3(followDifference.x * inputHorz, followDifference.y, followDifference.z);

    //fim do jogo
    public bool followFertilePatch;
    public Vector3 FPFollowDifference;
    Transform fertilePatchTransform;
    Vector3 fertilePatchPos;

    void Start(){
        if(player == null) GameObject.Find("Player").GetComponent<PlayerScript>();
    }

    void FixedUpdate(){
        if(IsFar && !followFertilePatch)
            DinamicFollow();

        if(followFertilePatch){
            GoToFertilePatch();
        }
    }

    #region Movement
    //Follows the player with a slight delay and can be in front of him
    void DinamicFollow(){
        playerPos = PlayerMovement._transform.position + _FollowDifference;

        if(speedMultiplier < 0) setPositive = -1; else setPositive = 1;

        smoothPos = Vector3.Lerp(transform.position, playerPos, (4 + speedMultiplier * setPositive) * Time.fixedDeltaTime);
        transform.position = smoothPos;
    }

    public void FoundFertilePatch(Transform patchTransform){
        fertilePatchTransform = patchTransform;
        followFertilePatch = true;
    }
    void GoToFertilePatch(){        
        fertilePatchPos = fertilePatchTransform.position + FPFollowDifference;

        if(speedMultiplier < 0) setPositive = -1; else setPositive = 1;

        smoothPos = Vector3.Lerp(transform.position, fertilePatchPos, (3 * setPositive) * Time.fixedDeltaTime);
        transform.position = smoothPos;
    }

    #endregion

    #region Sprite
    public void UpdateFlowerLife(int vida){
        
        //calcula vida
        maxLife = player.maxLife;
        CalculateSprite(vida);

        spriteRenderer.sprite = sprites.spritesFlor[spriteIndex];
    }

    void CalculateSprite(int vida){
        //spriteIndex = vida - (maxLife - healthPerSprite);
        int percentage = vida * 100 / maxLife;
        //para nao descer muito rapido
        percentage += difPraMudarSprite;

       // print(percentage.ToString() + " " + vida + " "  + maxLife.ToString());
        
        if(percentage > 0){
            spriteIndex = percentage/25;
        }
        else spriteIndex = 0;
        
        if(spriteIndex >= sprites.spritesFlor.Length){
            spriteIndex = sprites.spritesFlor.Length-1;
        }        
    }

    #endregion
}
