﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellShot : MonoBehaviour
{
    public bool debug;
    void Start(){
        Invoke("DestroySelf", 1.5f);
    }
    void DestroySelf(){
        Destroy(this.gameObject, 1f);
    }

    void OnTriggerEnter2D(Collider2D other){
        switch(other.gameObject.tag){
            case "Boss":
            case "Enemy": Destroy(this.gameObject, 0.02f);
            break;
            
            case "Tilemap": 
            case "Trap":
                Destroy(this.gameObject);
            break;

            case "DestroyOnShot":   Destroy(other.gameObject);
                                    Destroy(this.gameObject);
            break;
            
            default: if(debug) Debug.Log("Tiro colidiu com " + other.gameObject.name +". Nao faz nada.");
            break;
        }//Debug.Log("Tiro colidiu com " + other.gameObject.name +". Nao faz nada.");
    }
}
