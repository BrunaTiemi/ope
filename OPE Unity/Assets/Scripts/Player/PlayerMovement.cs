﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{    
    public PlayerScript player;
    public Rigidbody2D rididbody2d;

    public BoxCollider2D wallCheck, ceilingCheck;
    public float wallCheckDistance;

    public int facingDirection = 1; // 0 = esquerda, 1 = direita
    public float inputHorz, moveSpeed, jumpVelocity, wallSlideSpeed;
    [HideInInspector]
    public float runSpeed, walkSpeed = 1f;
    [SerializeField] private float wallJumpForce = 3;

    public bool isMoving => inputHorz != 0;
    public bool canMove;
    public bool facingLeft;
    public bool isFalling, isGrounded, canJump;
    public bool isTouchingWall, isTouchingCeiling, blockFlip;
    

    [Header("Jump")]
    public Collider2D playerCollider;
    [SerializeField] public LayerMask groundLayerMask, entityLayerMask; 
    
    [SerializeField] private bool canDoubleJump;
    public bool hasDoubleJump;

    
    public new Transform transform;
    public static Transform _transform;


    bool isAttacking => player.isAttacking;
    bool takingDamageAnim => player.takingDamageAnim;

    void Awake(){
        _transform = this.transform;
    }

    private void Start() {        
        canMove = true;
        hasDoubleJump = true;
        runSpeed = moveSpeed;
    }

    private void FixedUpdate() {        
        if(canMove) Movimento();
    }

    private void Update() {
        Jump();
    }

    #region Movement

    bool mouseToTheLeft => player.mainCamera.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x;
    void Movimento() {

        inputHorz = Input.GetAxisRaw("Horizontal");

        if(inputHorz < 0 && !facingLeft || inputHorz > 0 && facingLeft){
            Flip();
        }

        //if(mouseToTheLeft && !facingLeft || !mouseToTheLeft && facingLeft)
        //    Flip();

        //Move o player
        if(!isTouchingWall)
            rididbody2d.velocity = new Vector2(inputHorz*moveSpeed, rididbody2d.velocity.y);

        else if(!isTouchingGround && inputHorz != 0) {canDoubleJump = false;}

        //WallSlide
        if(isTouchingCeiling == false && WallSlide()){
            if(rididbody2d.velocity.y < -wallSlideSpeed){
                rididbody2d.velocity = new Vector2(rididbody2d.velocity.x, -wallSlideSpeed);
            }
        }
        //else AnimateIdle();

        //animacoes de correr
        if(isGrounded && !isAttacking && !takingDamageAnim){
            if(inputHorz == 0){
                player.Animate(PlayerScript.ANIM_IDLE);
            }
            else player.Animate(PlayerScript.ANIM_RUN);
        }
        else if(!isGrounded && !isAttacking && !takingDamageAnim){
            player.Animate(PlayerScript.ANIM_FALL);
        }
    }

    bool WallSlide() {
        if(isTouchingWall && !IsOnGround() && inputHorz != 0){ //se está na parede , não está no chão e está andando
            rididbody2d.velocity = new Vector2(rididbody2d.velocity.x, Mathf.Clamp(rididbody2d.velocity.y, -0.2f, float.MaxValue));
            //print("wallslide");
            canJump = false;
            player.Animate(PlayerScript.ANIM_FALL);
            return true;
        }
        else return false;
    }


    public void Walk(bool enableWalk){
        //toggle walk e run
        if(enableWalk){
            moveSpeed = walkSpeed;
        } else {
            moveSpeed = runSpeed;
        }
    }

    #endregion

    #region Checks
    //os dois sao ativados nos scripts Wallcheck e CeilingCheck
    public void TouchedWall(bool enable){
        //print(enable);
        if(enable == true){
            if( !isTouchingCeiling){
                isTouchingWall = enable;
            }
        }
        else isTouchingWall = false;    
    }
    public void TouchedCeiling(bool enable){
        isTouchingCeiling = enable;
    }

    #endregion
    
    void Jump() { //condicoes para pular

        //checa se esta no chao
        if(IsOnGround()){
            canDoubleJump = true;
            canJump = true;
        }

        if(Input.GetKey(KeyCode.Space)){
            if(canJump){
                ActualJump();
            } else {//se não pode pular
                if(Input.GetKeyDown(KeyCode.Space)){
                    
                    if(WallSlide()){ //======================= WALL JUMP ==============================
                        //aumenta a velocidade para pular mais alto
                        float tempVelocity = jumpVelocity;
                        jumpVelocity *= 0.5f;

                        //pulo
                        Vector2 wallJumpDir = new Vector2(3f, 6.5f);

                        Vector2 jumpForce = new Vector2(wallJumpForce * wallJumpDir.x * -facingDirection, wallJumpForce * wallJumpDir.y);

                        //tira a velocidade do player
                        rididbody2d.velocity = Vector2.zero;

                        //adiciona forca pro wall jump
                        rididbody2d.AddForce(jumpForce, ForceMode2D.Impulse);

                        //bloqueia e volta o movimento
                        StartCoroutine(WJStopMove());

                        //volta a velocidade para o normal
                        jumpVelocity = tempVelocity;
                    }                
                    //========================================== DOUBLE JUMP =============================
                    if(hasDoubleJump && canDoubleJump){
                        //aumenta a velocidade para pular mais alto
                        float tempVelocity = jumpVelocity;
                        jumpVelocity *= 1.6f;

                        ActualJump();
                        canDoubleJump = false;
                        //volta a velocidade para o normal
                        jumpVelocity = tempVelocity;
                    }
                }
            }
        }
        //quando solta o botao
        if(Input.GetKeyUp(KeyCode.Space)){
            if(!isAttacking) 
                player.Animate(PlayerScript.ANIM_FALL);

            canJump = false;
        }
    }

    #region Flip
    public void Flip() {
        if(!blockFlip){
            transform.Rotate(0f, 180f, 0f);
            player.habilidadeTransform.Rotate(0f, 180f, 0f); //para nao rodar junto
            facingLeft = !facingLeft;
            facingDirection *= -1;
            //(facingDirection);
        }
    }
    public void FlipAndBlock(){
        Flip();
        BlockFlip();
    }

    public void BlockFlip(){
        StartCoroutine(BlockFlipTimer());
    }
    IEnumerator BlockFlipTimer(){
        blockFlip = true;

        yield return new WaitForSeconds(0.3f);

        blockFlip = false;
    }


    public void FlipTowardsMouse(){
        if(mouseToTheLeft && !facingLeft || !mouseToTheLeft && facingLeft){
            FlipAndBlock();
        }
    }

    #endregion

    //o player nao consegue se mover no wall jump
    IEnumerator WJStopMove(){
        canMove = false;
        Flip();

        yield return new WaitForSeconds(0.3f);

        canMove = true;

    }
    void ActualJump() { //o que acontece quando pula
        rididbody2d.velocity = Vector2.up * jumpVelocity;
        isGrounded = false;
        if(!isAttacking)
        player.Animate(PlayerScript.ANIM_JUMP);
        StartCoroutine(JumpingTime());
    }
    IEnumerator JumpingTime() {
        yield return new WaitForSeconds(0.11f);
        canJump = false;

        yield return new WaitForSeconds(0.1f);
        isFalling = true;
    }   

    
    RaycastHit2D isTouchingGround => Physics2D.BoxCast(playerCollider.bounds.center, playerCollider.bounds.size, 0f, Vector2.down, 0.5f, groundLayerMask);
    
    RaycastHit2D isTouchingEntity => Physics2D.BoxCast(playerCollider.bounds.center, playerCollider.bounds.size, 0f, Vector2.down, 0.5f, entityLayerMask);
    private bool IsOnGround() { //checa se esta no chao para poder pular
        
        if(isTouchingGround || isTouchingEntity){
            isFalling = false;
            canJump = true;
            isGrounded = true;
            return true;
        } else { 
            isGrounded = false;
            return false;
        }
    }

    private void EnableCanMove() {
        canMove = true;
    }
}
