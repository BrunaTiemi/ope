﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
public enum Ability{
    None,
    Parry,
    WaterRay,
    TripleBalls,
    FollowingBall,
    MousePosAtk,
    Freeze
}

public class PlayerHabilidades : MonoBehaviour
{
    SaveGame saveGame;

    public PlayerScript playerScript;
    public PlayerAim playerAim;

    public bool hasWaterRay => saveGame.gameData.hasWaterRay;
    public bool hasTripleBalls => saveGame.gameData.hasTripleBalls;
    public bool hasFollowingBall => saveGame.gameData.hasFollowingBall;
    public bool hasMousePosAtk => saveGame.gameData.hasMousePosAtk;
    public bool hasFreeze => saveGame.gameData.hasFreeze;
    
    public bool endedAnim;

    [Range(0,100)]
    public float mana = 100, maxMana = 100;
    public float manaRegenRate = 0.2f; //quanto de mana vai ser regenerado em cada fixedupdate

    private string ANIM_ACTIVATE = "activate";

    [Header("Ray")]
    public Transform rayTransform;
    public Animator rayAnim;


    [Header("Difference")]
    public Vector3 rayYDif;


    //Aim towards mouse
    private Vector3 endPointPos => playerAim.endPointTransform.position;
    private Vector3 shootPos => playerAim.mousePosition;
    private Vector3 aimDir => playerAim.aimDir + rayYDif;
    private float shootAngle => Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;

    private void OnEnable() {
        GetAbilityTile.EnableAbility += EnableAbility;
        SaveGame.OnSaveLoaded += LoadAbilities;
    }
    private void OnDisable() {
        GetAbilityTile.EnableAbility -= EnableAbility;
        SaveGame.OnSaveLoaded -= LoadAbilities;
    }
    
    void EnableAbility(Ability ability){
        saveGame.gameData.EnableAbility(ability);
    }

    private void Start() {
        maxMana = 100;
        mana = maxMana;
    }

    private void Update() {
        
        if(Input.GetKeyDown(KeyCode.E)){
            if(hasWaterRay){
                WaterRay();
            }
        }

        //if(GameManager.testes){
        //    hasWaterRay = true;
        //}
    }

    private void FixedUpdate() {
        if(mana < maxMana){
            mana += manaRegenRate;
        }
    }

    private void LoadAbilities(SaveGame save){
        saveGame = save;
    }

    private void WaterRay(){
        //Debug.DrawLine(rayTransform.position, shootPos, Color.blue, 0.5f);

        float manaCost = 20;

        //se nao tem mana, nao roda. Se tem, a usa. e prossegue
        if(!TryUseMana(manaCost)) return;
        
        rayTransform.eulerAngles = new Vector3(0f, 0f, shootAngle);
        rayAnim.SetTrigger(ANIM_ACTIVATE);
        //print("RAY");        
    }

    public void Freeze(Vector3 aimDir){
        //se nao tem a habilidade, nao pode usar
        if(!hasFreeze) return;
        print("temf");

        //se tem, ve se tem mana o bastante
        float manaCost = 60; // tres quintos de 100, usa 3 bolinhas de mana
        if(!TryUseMana(manaCost)) return;
        print("mana");

        //usa a habilidade
        playerScript.IcePosTiro(aimDir);
    }

    #region Mana    
    bool TryUseMana(float manaCost){
        //se nao tem mana o suficiente, retorna false
        if(!HasEnoughMana(manaCost)) return false;

        //se tem, usa a mana e retorna true
        UseMana(manaCost);
        return true;
    }

    bool HasEnoughMana(float manaCost){
        bool canUse = mana >= manaCost;
        if(!canUse) print("nao tem mana");
        return canUse;
    }

    void UseMana(float manaCost){
        mana -= manaCost;
    }

    #endregion
}
