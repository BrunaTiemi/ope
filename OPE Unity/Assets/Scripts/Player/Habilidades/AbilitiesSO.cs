﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "AbilitiesSO", menuName = "OPE/AbilitiesSO")]
public class AbilitiesSO : ScriptableObject {
    public Hab[] habilidades;

    public string GetKeyToUseText(Ability ability){
        //pega a habilidade correspondente
        int index = Array.FindIndex(habilidades, x => x.abilType == ability);

        //retorna o texto na lingua certa
        return habilidades[index].keyToUseText[Array.FindIndex(habilidades[index].keyToUseText, x => x.language == LanguageManager.currentLanguage)].text;

    }

    public string GetOpenInvText(){
        //instrucao de abrir o inventario
        switch (LanguageManager.currentLanguage)
        {
            case Language.Portuguese: return "Aperte 'I' para abrir o inventário!";

            default: return "Pres 'I' to open your inventory!";
        }
    }

    public Ability GetAbilityByStage(int nivel, int fase){
        List <Hab> habilidadesPossiveis = new List<Hab>();

        Ability abilityToReturn = Ability.Parry; //eh importante default pra parry pq ele sempre chama essa funcao se tiver parry
        Debug.Log("aAAAAAAAAAAAAAAA" + nivel + fase);
        //procura pelo nivel
        for (int i = 0; i < habilidades.Length; i++)
        {
            if(habilidades[i].nivel == nivel){
                habilidadesPossiveis.Add(habilidades[i]);
                Debug.Log("possivel" + habilidades[i].abilType);
            }
        }
        //procura pela fase
        for (int i = 0; i < habilidadesPossiveis.Count; i++)
        {
            if(habilidadesPossiveis[i].fase == fase){
                abilityToReturn = habilidadesPossiveis[i].abilType;
            }
        }
        Debug.Log("abiliidoderson" + abilityToReturn);
        
        return abilityToReturn;
    }

    [System.Serializable]
    public class Hab{
        [Header("-------")]
        public Ability abilType;
        public LanguageText[] keyToUseText;
        [Tooltip("Quanto a habilidade vai ser recebida?")]
        public int nivel, fase;
    }

}