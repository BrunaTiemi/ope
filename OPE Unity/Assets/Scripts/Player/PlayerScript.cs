﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerScript : MonoBehaviour
{
    public GameDifficultyScriptableObject dificuldade;
    public FlowerScript flower;

    public PlayerMovement movement; //script that handles movement
    public Camera mainCamera;

    //player
    public float vida;
    public int maxLife = 20;
    private Animator anim;
    private string currentAnimState;
    private float invencivelAte;
    public bool canSwitchAnim = true;

    public static event Action PlayerRespawn;
    
    public Inventory inventory;    

    
    //Dano
    [Header("Dano")]
    public GameObject shield;
    private float lookingUpDown; //up = 1, down = -1, none = 0;
    [SerializeField] private bool swordCanDamage, canShoot = true, invencivel;
    public bool isAttacking, hasShield, isUsingAbility, isDead;
    public float shieldTime;
    [SerializeField] float shieldCurrentTime;
    Vector3 shootDir, shootPos;
    [SerializeField] float tempoInv = 0.5f;

    //Attacks
    GameObject currentShot; //qual o tiro que vai usar agora, de agua ou de gelo
    public GameObject waterBall, iceBall;
    public float shotSpeed;
    public Transform shootTransform;
    public Collider2D espada;


    [Header("Habilidades")]
    //Habilidades
    public Transform habilidadeTransform; //para nao rodar junto com o player
    public bool hasParry, parry;


    [Header("Outros")]
    
    
    //Checkpoints
    [SerializeField]
    private Vector3 lastCheckpoint;
    private float healthAtCheckpoint;

    //Animacoes
    public bool takingDamageAnim;

    public const string ANIM_IDLE = "Idle";
    public const string ANIM_RUN = "Run";
    public const string ANIM_JUMP = "Jump";
    public const string ANIM_FALL = "Fall";
    public const string ANIM_TAKEDAMAGE = "TakingDamage";
    public const string ANIM_PARRY = "Parry";
    public const string ANIM_ATK1 = "Atk1";
    public const string ANIM_ATK2 = "Atk2";
    public const string ANIM_RANGEDATK = "RangedAtk";
    public const string ANIM_RANGEDATK_RUN = "RangedAtkRun";

    //sons
    public SoundEffects soundEffects;

    void Start() {
        GetDifficulty();

        vida = maxLife;

        anim = GetComponent<Animator>();

        lastCheckpoint = transform.position;

        //eventos
        SubscribeToEvents();

    }

    void GetDifficulty(){
        dificuldade.LoadDifficulty();

        maxLife = dificuldade.maxLife;
        tempoInv = dificuldade.invincibleTime;
        shieldTime = dificuldade.shieldTime;
    }

    void MesmoNivel(){
        //vida = (int)PlayerPrefs.GetFloat("LastStageHealth");
       // UpdateHealth();
    }
    private void SubscribeToEvents(){        
        EnemyClass.damagePlayer += LevaDano;
        enemyShot.hitPlayer += LevaDano;
        DamagePlayer.dmgPlayer += LevaDano;
        GameManager.MesmoNivel += MesmoNivel;
        SentryScript.DamagePlayer += LevaDano;
    }

    private void OnDestroy() {
        EnemyClass.damagePlayer -= LevaDano;
        enemyShot.hitPlayer -= LevaDano;
        DamagePlayer.dmgPlayer -= LevaDano;
        GameManager.MesmoNivel -= MesmoNivel;
        SentryScript.DamagePlayer -= LevaDano;
    }

    void Testes() {
        movement.hasDoubleJump = true;
        hasParry = true;
        vida = 20;

        //pulo infinito
        if(Input.GetKey(KeyCode.W)){
            movement.rididbody2d.velocity = Vector2.up * movement.jumpVelocity;
            if(!isAttacking)
                Animate(ANIM_JUMP);
        }
        if(Input.GetKeyDown(KeyCode.B)){
            parry = !parry;
        }

        if(Input.GetKeyDown(KeyCode.I)){
            print(PlayerMovement._transform.position);
        }
    }
    void Update() {
        Ataques();
        Habilidades();
        //Wall Jump
        //isTouchingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckDistance, groundLayerMask);
        //isTouchingCeiling = Physics2D.OverlapCircle(ceilingCheck.position, wallCheckDistance, groundLayerMask);
        
        if(GameManager.testes) Testes();
    }
    private void FixedUpdate() {
        if(transform.position.y < -30f){ 
            Debug.Log("Caiu do mundo");
            Respawn();
        }

        if(hasShield == true){
            shieldCurrentTime -= Time.deltaTime;
            if(shieldCurrentTime <= 0){
                hasShield = false;
                shield.SetActive(false);
            }
        }
        
        //Olhando para cima ou para baixo
        lookingUpDown = Input.GetAxisRaw("Vertical");

    }

    
    public void TeleportPlayer(float x, float y){
        PlayerMovement._transform.position = new Vector3(x, y, 0);
    }

    #region ATK
    void Ataques() { //isAtacking eh ativado e desativado na animacao
        //Melee Atk
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            if(currentAnimState != ANIM_ATK1){
                Animate(ANIM_ATK1);
                movement.FlipTowardsMouse();
            }
            else{
                Animate(ANIM_ATK2);
                movement.FlipTowardsMouse();
            }
            soundEffects.PlaySoundEffect(0);
        }
    }

    void AtaquesAntigos(){
        //Ranged Atk
        if(Input.GetKeyDown(KeyCode.X)){ 
            if(canShoot){
                Animate(ANIM_RANGEDATK); //Will trigger instantiate
                ShootDelay(0.05f);

            }
        }

        //Melee Atk
        if(Input.GetKeyDown(KeyCode.Z)){
            if(currentAnimState != ANIM_ATK1){
                Animate(ANIM_ATK1);
                movement.FlipTowardsMouse();
            }

            else{
                Animate(ANIM_ATK2);
                movement.FlipTowardsMouse();
            }
        }
    }

    public void StartedUsingAbility(){
        StartCoroutine(UsingAbilityCoroutine());
    }
    IEnumerator UsingAbilityCoroutine(){ //pro ataque n ser interrompido e perdido
        isUsingAbility = true;

        yield return new WaitForSeconds(0.5f);

        isUsingAbility = false;
    }

    public Vector3 shootDif;
    public void WaterPosTiro(Vector3 dir){
        currentShot = waterBall; // muda o tiro atual para o tiro de agua
        PegaPosTiro(dir);
    }

    bool atirouGelo;
    public void IcePosTiro(Vector3 dir){
        atirouGelo = false;

        StartedUsingAbility();
        currentShot = iceBall; // muda o tiro atual para o tiro de gelo
        PegaPosTiro(dir);
    }

    public void PegaPosTiro(Vector3 dir){
        shootDir = dir + shootDif;

        //muda a direcao do player de acordo com a pos do mouse
        movement.FlipTowardsMouse();

        Animate(ANIM_RANGEDATK); // se nao, toca a aniimacao
    }

    void InstantiateTiro(){ //start pos - centro do player. endpos - final da mira
        GameObject tiro = Instantiate(currentShot, shootTransform.position, Quaternion.identity);
        Rigidbody2D tiroRb = tiro.GetComponent<Rigidbody2D>();
        float moveSpeed = 800f;
        tiroRb.AddForce(shootDir * moveSpeed, ForceMode2D.Impulse);
        atirouGelo = true;

        soundEffects.PlaySoundEffect(0);
    }
    public void InstantiateTiro4Dir() { //atira com X
        GameObject tiro = Instantiate(waterBall, shootTransform.position, Quaternion.identity);
        Rigidbody2D rb = tiro.GetComponent<Rigidbody2D>();

        if(lookingUpDown == 0 && movement.facingLeft){ //atirar para a esquerda
            rb.velocity = new Vector2(-shotSpeed, 0);
        }
        else if(lookingUpDown == 0 && !movement.facingLeft){ //se nao esta olhando para cima / esta olhando para a direita
            rb.velocity = new Vector2(shotSpeed, 0);
        }
        else if(lookingUpDown == 1){ //se olhando para cima
            rb.velocity = new Vector2(0, shotSpeed);
        }
        else // olhando para baixo
            rb.velocity = new Vector2(0, -shotSpeed);
    }

    IEnumerator ShootDelay(float time){
        canShoot = false;
        yield return new WaitForSeconds(time);
        canShoot = true;
    }

    public void AtivarColliderEspada() {
        espada.enabled = true;
    }
    public void DesativarColliderEspada() {
        espada.enabled = false;
    }

    void IsAttacking(){ //Ativado e desativado nas animacoes
        isAttacking = true;
        Invoke("FinishedAttacking", 0.3f); //para nao ficar preso no true
    }
    void FinishedAttacking(){ //Ativado e desativado nas animacoes
        isAttacking = false;
        CancelInvoke("FinishedAttacking");
    }
    
    #endregion

    #region Vida
    public void LevaDano(int dano){
        if(Time.time >= invencivelAte){
            invencivel = false;
            takingDamageAnim = false;
        }
        
        if(parry){
        }
        else if(!invencivel && !hasShield){ //se não estiver invencivel ou com escudo, leva dano
            vida -= dano;
            Animate(ANIM_TAKEDAMAGE);
            invencivel = true;
            invencivelAte = Time.time + tempoInv;
            soundEffects.PlaySoundEffect(1);
        }
        
        UpdateHealth();
        
        //Morre
        if(vida < 1){
            Die();
        }
    }
    void Die(){
        if(isDead == false){
            //desliga o movimento e toca a animacao
            isDead = true;
            movement.canMove = false;
            anim.SetTrigger("Death");
        }
    }

    public void AddVida(int num){
        //se o resultado for menor ou igual a vida maxima, adiciona
        if((vida + num ) <= maxLife){
            vida += num;
            UpdateHealth();
        }
        //se for maior que a vida maxima, vida = vida maxima
        else vida = maxLife;
    }

    void UpdateHealth(){
        flower.UpdateFlowerLife((int)vida);
    }

    void Respawn() {
        //volta para o ultimo checkpoint
        PlayerMovement._transform.position = lastCheckpoint;
        
        //se healthAtCheckpoint for = 0, vida = maxlife
        //vida = healthAtCheckpoint == 0 ? maxLife : healthAtCheckpoint;    //antes salvava a vida do player ao passar no checkpoint
        vida = maxLife;                                                     //agora a vida eh cheia ao dar respawn em um checkpoint
        
        UpdateHealth();

        if(PlayerRespawn != null) PlayerRespawn();

        isDead = false;
        movement.canMove = true;
    }

    #endregion

    #region Items

    public void GotShield(){
        hasShield = true;
        shieldCurrentTime = shieldTime;
        shield.SetActive(true); //eh desativado dentro do FixedUpdate()
    }

    #endregion

    #region Animations
    public void Animate(string newState){
        if(!isDead && canSwitchAnim){
            if(!isUsingAbility){
                //para nao trocar de animacao enquanto esta usando uma habilidade, caso ela seja necessaria
                if(isUsingAbility) return;

                //para nao tocar a animacao em cima dela mesma
                if(currentAnimState == newState) return;

                //para nao trocar de animacao enquanto estiver atirando pra n parar o tiro
                if(currentAnimState == ANIM_RANGEDATK && newState == ANIM_FALL && isAttacking) return;

                //se estiver correndo, usa a animacao de correr
                if(newState == ANIM_RANGEDATK && movement.isMoving) {
                    newState = ANIM_RANGEDATK_RUN;
                print("correndo");
                }
            }

            //toca a animacao e troca o currentAnimState
            if(anim != null) anim.Play(newState);
            currentAnimState = newState;
        }
    }
    public void AllowAnimationSwitch(){
        canSwitchAnim = true;
    }
    public void DisallowAnimationSwitch(){
        canSwitchAnim = false;
    }

    void AnimationHandler(){
    }

    void Flip(){
        movement.Flip();
    }

    #endregion


    #region Habilidades

    void Habilidades() {
        if(Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.LeftShift)){
            if(hasParry){
                parry = true;
                Animate(ANIM_PARRY);
                StartCoroutine(ParryCooldown());
            }
        }
    }
    IEnumerator ParryCooldown() {
        parry = true;
        yield return new WaitForSeconds(0.4f);
        parry = false;
        isAttacking = false;
    }

    #endregion

    #region Collisions
    private void OnTriggerEnter2D(Collider2D other) {
        switch(other.gameObject.tag){
            //nao leva dano se o outro tiver a tag boss, pra poder dar dano sem levar dano quando encostar

            case "DirectionCollider": 
            break;
             
            case "Checkpoint":  
                //salva posicao do checkpoint
                lastCheckpoint = new Vector3(other.transform.position.x, other.transform.position.y, 0f);

                bool sameCheckpoint = other.GetComponent<Checkpoint>().isActive;
                //se esta no mesmo checkpoint ativado e a vida salva do checkpoint eh menor do que a atual, salva a vida atual como do checkpoint
                if(sameCheckpoint){
                    if(healthAtCheckpoint < vida){
                        healthAtCheckpoint = vida;
                    }
                }
                else{
                    vida += 2;
                    healthAtCheckpoint = vida;
                }
            break;

            case "DamagePlayer": LevaDano(2);
            break;

            case "StrongDamagePlayer": LevaDano(3);
            break;

            case "ItemVida": AddVida(3);
                             Destroy(other.gameObject);
            break;

            case "ItemShield": GotShield();
                               Destroy(other.gameObject);
            break;

            default:
            break;
        }    
    }

    private void OnTriggerStay2D(Collider2D other) {
        switch(other.gameObject.tag){
            case "DamagePlayer":
                LevaDano(1);
            break;

            case "Enemy":
                LevaDano(1);
            break;
        }
    }

    #endregion

    private void OnDrawGizmos() {
        //Vector3 rightEndPos = new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z);
        //Vector3 leftEndPos = new Vector3(wallCheck.position.x - wallCheckDistance,  wallCheck.position.y, wallCheck.position.z);

       // Gizmos.color = Color.red;

        //WallCheck
        //if(!movement.facingLeft){
        //    Gizmos.DrawLine(wallCheck.position, rightEndPos);
        //}
        //else Gizmos.DrawLine(wallCheck.position, leftEndPos);

        //GroundCheck
        //Debug.DrawRay(movement.playerCollider.bounds.center + new Vector3(movement.playerCollider.bounds.extents.x, 0), Vector2.down * (movement.playerCollider.bounds.extents.y + 0.3f), Color.red);
        //Debug.DrawRay(movement.playerCollider.bounds.center - new Vector3(movement.playerCollider.bounds.extents.x, 0), Vector2.down * (movement.playerCollider.bounds.extents.y + 0.3f), Color.red);
        //Debug.DrawRay(movement.playerCollider.bounds.center - new Vector3(0, movement.playerCollider.bounds.extents.x, movement.playerCollider.bounds.extents.y + 0.3f), Vector2.right * (movement.playerCollider.bounds.extents.y), Color.red);
    
        //CeilingCheck
        //Vector3 upEndPos = new Vector3(ceilingCheck.position.x, ceilingCheck.position.y + 0.29f, ceilingCheck.position.z);
        //Gizmos.DrawLine(ceilingCheck.position, upEndPos);
    }
}
