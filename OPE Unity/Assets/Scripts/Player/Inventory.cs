﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public CartasSO cartasSO;
    private SaveGame save => GameObject.FindObjectOfType<SaveGame>();

    public int keys;
    public bool[] cartas;

    public bool delay; //delay para nao pegar varios do mesmo item.

    private void Start() {
        cartas = new bool[cartasSO.cartas.Length];
        //inicializa o save das cartas
        if(save.lettersData.cartas.Count < cartasSO.cartas.Length){
            save.lettersData.InicializaCartas(cartas.Length);
        }
        cartasSO.SetTitulos();
    }

    public void AddItem(Item item ){
        switch(item){
            case Item.key: 
                if(!delay){
                    Delay();
                    keys++;
                }
                break;
        }
    }

    public void AddLetter(int id){
        save.lettersData.PegouCarta(id);
        if(cartas.Length > id) cartas[id] = true;
    }

    public void RemoveItem(Item item ){
        switch(item){
            case Item.key: 
                if(!delay){
                    Delay();
                    keys--;
                }
                break;
        }
    }

    private void Delay(){
        StartCoroutine(DelayCoroutine());
    }

    IEnumerator DelayCoroutine(){
        delay = true;

        yield return new WaitForSeconds(0.1f);

        delay = false;        
    }

    private void OnTriggerEnter2D(Collider2D other) {        
        switch(other.gameObject.tag){                        
            case "Key": //nao colocar chaves muito perto uma das outras pq o delay pode fuder
                AddItem(Item.key);
                Destroy(other.gameObject);
            break;

            //case "Carta":
            //    //pega o id da carta e adiciona no inventario 
            //    CartaScript cs = other.GetComponent<CartaScript>();
            //    AddLetter(cs.id);
            //    cs.MostrarEscrita();
            //    break;
        }
    }
}

public enum Item{
    key,
    carta
}