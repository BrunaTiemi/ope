﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerAim : MonoBehaviour
{
    public Camera mainCamera;
    public Transform aimPosition;
    public Transform endPointTransform, startPointTransform;
    public Vector3 mousePosition, aimDir;
    PlayerScript playerScript;
    PlayerHabilidades playerHabilidades;

    //public static event Action<Vector3, Vector3> OnShoot;

    private void Start() {
        if(mainCamera == null)
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        playerScript = GetComponent<PlayerScript>();
        playerHabilidades = GetComponentInChildren<PlayerHabilidades>();
    }
    private void Update() {
        PegarValores();
        Atirar();
    }

    void PegarValores(){
        //pega posicoes da mira e seta a direcao da mira
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        aimDir = (mousePosition - transform.position).normalized;
    }

    void Atirar(){

        if(Input.GetMouseButtonDown(1)){
            playerScript.WaterPosTiro(aimDir);

            //OnShoot(endPointTransform.position, mousePosition);
        }
        if(Input.GetKeyDown(KeyCode.Q)){
            playerHabilidades.Freeze(aimDir);            
        }
    }
}
