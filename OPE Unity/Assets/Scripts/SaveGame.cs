﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SaveGame : MonoBehaviour
{
    public GameData gameData;
    public LettersData lettersData;
    public string json, lettersJson;

    public static event Action<SaveGame> OnSaveLoaded; 

    private void Awake() {
        LoadData(this.gameObject.name);

        OnSaveLoaded?.Invoke(this);
        /*GameData gameData = new GameData();
        gameData.nivel = 1;
        gameData.fase = 1;
        gameData.inimigosMortos = 0;

        string json = JsonUtility.ToJson(gameData);
        Debug.Log(json);
        */
    }

    private void Start() {        
        OnSaveLoaded?.Invoke(this);
    }

    public bool IsLoaded(){
        if(gameData == null){
            LoadData(this.gameObject.name);
        }
        if(lettersData == null){
            LoadLettersData();
        }

        return true;
    }

    public void LoadData(string senderName){
        //carrega do save
        json = File.ReadAllText(Application.dataPath + "/game_data.json");
        if(json == "") CreateJson();
        else gameData = JsonUtility.FromJson<GameData>(json);

        LoadLettersData();

        //Debug.Log("Loaded Data");
        //Debug.Log(json);

        //Debug.Log("Sender: " + senderName);
    }

    void LoadLettersData(){
        lettersJson = File.ReadAllText(Application.dataPath + "/letters_data.json");
        
        if(lettersJson == "") CreateLettersJson();
        else lettersData = JsonUtility.FromJson<LettersData>(lettersJson);

        //Debug.Log("Loaded Letters Data");
        //Debug.Log(json);
    }

    public void CreateJson(){
        gameData = new GameData();
        
        json = JsonUtility.ToJson(gameData);
    }
    public void CreateLettersJson(){        
        lettersData = new LettersData();
        
        lettersJson = JsonUtility.ToJson(lettersData);
    }

    public void RestartCheckpoint(){
        gameData.lastCheckpoint = 0;
        SaveData(gameObject.name);
    }

    public void SaveData(string senderName){
        Debug.Log(json);
        json = JsonUtility.ToJson(gameData);
        //salva nos arquivos
        File.WriteAllText(Application.dataPath + "/game_data.json", json);
        SaveLetterData();

        //Debug.Log("Saved Data");
       // Debug.Log(json);
        
        //Debug.Log("Sender: " + senderName);
    }

    public void SaveLetterData(){
        lettersJson = JsonUtility.ToJson(lettersData);

        //salva nos arquivos
        File.WriteAllText(Application.dataPath + "/letters_data.json", lettersJson);
    }

    public void DeleteAllData(){
        gameData.ResetAllData();
        lettersData.SalvarCartasDoZero();
        SaveData(this.gameObject.name);
        Debug.Log("Deleted all data");
    }

    public void DeleteAllDataAndPP(){
        DeleteAllData();
        PlayerPrefs.DeleteAll();
    }
}
[System.Serializable]
    public class GameData{
        public int nivel;
        public int fase;
        public int lastCheckpoint;

        public int inimigosMortos;

        public bool dirty; //nao esta sendo usado ainda

        public GameData(){            
            nivel = 0;
            fase = 0;
            lastCheckpoint = 0;
            inimigosMortos = 0;
            dirty = false;

            ResetAllAbilities();
        }

        public void ResetAllData(){
            dirty = true;

            //reseta todos os niveis e checkpoints
            ResetLevelsButKeepAbilities();

            //reseta todas as habilidades
            ResetAllAbilities();
        }

        public void ResetLevelsButKeepAbilities(){
            dirty = true;

            nivel = 0;
            fase = 0;
            lastCheckpoint = 0;
            inimigosMortos = 0;
        }

        public void SaveCheckpoint(int value){
            dirty = true;

            lastCheckpoint = value;
        }

        public void AddDeadEnemies(int value){
            dirty = true;
            
            inimigosMortos += value;
        }

    #region Habilidades        
        public bool hasParry; //defletir ataques
        public bool hasWaterRay; //raio laser de agua
        public bool hasTripleBalls; //tres bolas de agua
        public bool hasFollowingBall; //ataque que segue o inimigo mais perto
        public bool hasMousePosAtk; //ataque que eh ativado na posicao do mouse.
        public bool hasFreeze; //ataque que congela os inimigos

        public void ResetAllAbilities(){
            hasParry = true;
            hasWaterRay = false;
            hasTripleBalls = false;
            hasFollowingBall = false;
            hasMousePosAtk = false;
            hasFreeze = false;
        }

        public void EnableAbility(Ability ability){
            switch(ability){                    
                case Ability.WaterRay: hasWaterRay = true;
                break;
                
                case Ability.TripleBalls: hasTripleBalls = true;
                break;

                case Ability.FollowingBall: hasFollowingBall = true;
                break;

                case Ability.MousePosAtk: hasMousePosAtk = true;
                break;

                case Ability.Freeze: hasFreeze = true;
                break;
            }
            //vai ser salvo soh quando chegar em um checkpoint
        }

    #endregion
    }

    [System.Serializable]
    public class LettersData{
        public List<bool> cartas;

        public LettersData(){
            cartas = new List<bool>();
        }

        public void InicializaCartas(int qtd){
            //tira a quantidade de cartas que ja tem pra nao adicionar demais
            qtd -= cartas.Count;

            //adiciona as cartas restantes
            for (int i = 0; i < qtd; i++)
            {
                cartas.Add(false);
            }
        }

        //para quando adicionar mais cartas
        public void SalvarCartasDoZero(){
            cartas = new List<bool>();
        }

        public void PegouCarta(int id){
            if(cartas.Count > id) cartas[id] = true;
        }
    }

