﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    SaveGame save;
    public GameObject askLangObj, mainMenuObj, dotsTalePanel;
    public GameObject[] dontShowFirstTime;

    public GameObject[] mostrarSeNaoTemSave, mostrarSeTemSave;

    public GameObject selecaoFases;
    public int firstTime;
    //se ja comecou a jogar e passou de um checkpoint, para dar continue
    public bool temSave => save.gameData.fase > 1 || save.gameData.nivel > 1 || save.gameData.lastCheckpoint != 0;

    void Start()
    {
        firstTime = PlayerPrefs.GetInt("FirstTime", 1);

        //se acabou o jogo, ativa a selecao de fases
        if(PlayerPrefs.GetInt("AcabouJogo", 0) == 1) selecaoFases.SetActive(true);

        save = GetComponent<SaveGame>();
        if(save.IsLoaded()){}

        if(firstTime == 1){
            askLangObj.SetActive(true);
            mainMenuObj.SetActive(false);
            PlayerPrefs.SetInt("FirstTime", 0);

            for(int i = 0; i< dontShowFirstTime.Length; i++){
                dontShowFirstTime[i].SetActive(false);
            }
        }
        else{
            mainMenuObj.SetActive(true);
            dotsTalePanel.SetActive(true);
            askLangObj.SetActive(false);

            if(temSave){
                MostrarObjetos(mostrarSeTemSave, true);
                MostrarObjetos(mostrarSeNaoTemSave, false);
            }
            else {
                MostrarObjetos(mostrarSeTemSave, false);
                MostrarObjetos(mostrarSeNaoTemSave, true);
            }
        }
    }

    void MostrarObjetos(GameObject[] objetos, bool mostrar){        
        for (int i = 0; i < objetos.Length; i++)
        {
            objetos[i].SetActive(mostrar);
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.End)){
            selecaoFases.SetActive(true);
        }
    }
    
    public void StartGame(){
        //se ja tem save, deleta tudo
        if(temSave) save.DeleteAllData();

        
        GoToScene("Historia");
    }

    public void ContinueGame(){
        int nivel = save.gameData.nivel;
        string fase = $"fase{save.gameData.fase}";

        if(fase == "fase4") fase = "BossFight";

        GoToScene($"nivel{nivel}{fase}");

    }

    public void GoToScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
}
