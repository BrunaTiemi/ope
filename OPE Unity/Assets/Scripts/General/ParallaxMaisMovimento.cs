﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxMaisMovimento : MonoBehaviour
{   ///
    ///Usado para adicionar um movimento constante a um objeto que tem parallax *fumaca que sempre anda pro lado
    ///
    public Vector2 parallaxMultiplier;
    Transform camTransform;
    Vector3 lastCamPos;
    float imgWidth;
    public float constMoveSpeed; //para adicionar um movimento constante alem do parallax

    Vector3 deltaMovement => camTransform.position - lastCamPos;
    Vector3 sumPos => new Vector3(deltaMovement.x * parallaxMultiplier.x, deltaMovement.y * parallaxMultiplier.y, deltaMovement.z);
    Vector3 constantMovement => new Vector3(constMoveSpeed, 0f, deltaMovement.z);

    private void Start() {
        camTransform = Camera.main.transform;
        lastCamPos = camTransform.position;
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        imgWidth = sprite.texture.width/sprite.pixelsPerUnit;
    }

    private void FixedUpdate() {
        //muda a posicao do fundo
        transform.position += sumPos;
        lastCamPos = camTransform.position;
    
        //copiar os sprites infinitamente
        if(Mathf.Abs(camTransform.position.x - transform.position.x) >= imgWidth){
            float offsetPositionX = (camTransform.position.x - transform.position.x) % imgWidth;
            transform.position = new Vector3(camTransform.position.x + offsetPositionX, transform.position.y);
        }

        transform.position += constantMovement;
    }
}
