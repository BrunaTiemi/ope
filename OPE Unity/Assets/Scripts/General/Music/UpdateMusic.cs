﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMusic : MonoBehaviour
{
    void Start()
    {
        GameObject.FindObjectOfType<MusicHandler>().GetNewTrack();
    }
}
