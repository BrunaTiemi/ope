﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MusicHandler : MonoBehaviour
{
    public static MusicHandler instance;
    public AudioSource musicSource;

    public MusicScriptableObject musicSO;
    public float musicVolume = 1f;

    private string currentScene, lastScene;

    public int currentStage, lastStage;

    bool isThisTheInstance;

    private void Awake(){
        //se achar mais de um objeto de musica na cena, se destroi.
        MusicHandler[] musicObj = GameObject.FindObjectsOfType<MusicHandler>();
        print($"music handlers {musicObj.Length}");
        if(musicObj.Length > 1){
            Destroy(this.gameObject);
        }
        else{
            isThisTheInstance = true;
            //se nao, esse nao eh destruido
            DontDestroyOnLoad(this.gameObject);
            
            lastStage = 0;

            //seta o volume e pega a musica
            musicVolume = PlayerPrefs.GetFloat("BGVolume", 0.5f);;
            musicSource.volume = musicVolume;
            GetNewTrack();
            print("AWAKE");
        }
    }
    private void OnEnable() {        
        SceneManager.activeSceneChanged += MudouDeCena;
        ChangeSoundVolume.OnVolumeChanged += SetVolume;
    }
    private void OnDisable() {        
        SceneManager.activeSceneChanged -= MudouDeCena;
        GameManager.OnSceneChanged -= GetNewTrack;        
        ChangeSoundVolume.OnVolumeChanged -= SetVolume;
    }
    void MudouDeCena(Scene a, Scene b){
        GetNewTrack();
    }
    public void GetNewTrack(){
        if(isThisTheInstance){
            AudioClip newClip = musicSource.clip;


            currentScene = SceneManager.GetActiveScene().name;
        
            if(!currentScene.Contains("nivel")){
                //se estiver no menu
                if(currentScene.Contains("MainMenu")){
                    newClip = musicSO.mainMenuTrack;
                } else if (currentScene.Contains("Historia")) newClip = musicSO.historiaTrack;
                else if (currentScene.Contains("Creditos")) newClip = musicSO.creditosTrack;
            }

            //se estiver em algum nivel
            if(currentScene.Contains("nivel") || currentScene.Contains("Nivel")){
                //pega a fase pelo nome da cena
                currentStage = (int)Char.GetNumericValue(currentScene[5]);
                print( $"currentStage {currentStage} num {(int)Char.GetNumericValue(currentScene[5])}");
                
                //se estiver no boss, pega a musica dele
                if(currentScene.Contains("Boss")) newClip = musicSO.trackIndexBoss(currentStage);

                print($"current {currentStage} last {lastStage} ");
                //pega qual musica tocar pelo index
                if(currentScene.Contains("fase")) newClip = musicSO.trackIndex(currentStage);
            }

            if(currentScene == "nivel1fase1" && newClip != musicSO.trackIndex(1)) newClip = musicSO.trackIndex(1);

            if(newClip != musicSource.clip) {
                musicSource.clip = newClip;
                musicSource.Play();
                Debug.Log("muda de musica");
            }
        

            lastScene = currentScene;
            lastStage = currentStage;
        }
    }

    public void SetVolume(float newVol){
        musicVolume = newVol;
        musicSource.volume = musicVolume;
        PlayerPrefs.SetFloat("BGVolume", musicVolume);
    }
}
