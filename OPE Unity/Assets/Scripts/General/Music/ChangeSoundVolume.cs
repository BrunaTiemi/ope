﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChangeSoundVolume : MonoBehaviour
{
    public static event Action<float> OnVolumeChanged;
    public void ChangeVolume(float newVol){
        OnVolumeChanged(newVol);
    }
}
