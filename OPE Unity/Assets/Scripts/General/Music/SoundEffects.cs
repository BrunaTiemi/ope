﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    public MusicScriptableObject musicSO;

    public AudioSource effectsAudioSource;

    public float volume;

    private void Start() {
        volume = PlayerPrefs.GetFloat("BGVolume", 0.5f);
        effectsAudioSource.volume = volume;
    }

    private void OnEnable() {        
        ChangeSoundVolume.OnVolumeChanged += SetVolume;
    }
    private void OnDisable() {        
        ChangeSoundVolume.OnVolumeChanged -= SetVolume;
    }


    public void PlaySoundEffect(int num){
        effectsAudioSource.clip = musicSO.soundEffects[num];
        effectsAudioSource.Play();
    }
    public void SetVolume(float newVol){
        volume = newVol;
        effectsAudioSource.volume = volume;
        PlayerPrefs.SetFloat("BGVolume", volume);
    }
}
