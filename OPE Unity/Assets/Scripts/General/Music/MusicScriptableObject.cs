﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MusicSO", menuName = "Sounds/MusicScriptableObject", order = 0)]
public class MusicScriptableObject : ScriptableObject {
    [Header("Niveis")]
    public AudioClip desertTrack;
    public AudioClip cityTrack;
    public AudioClip waterTrack;
    public AudioClip forestTrack;

    [Space][Header("Bosses")]
    public AudioClip desertBoss;
    public AudioClip cityBoss, waterBoss, forestBoss;

    [Space][Header("Menus")]
    public AudioClip mainMenuTrack;
    public AudioClip creditosTrack, historiaTrack;

    public AudioClip[] soundEffects;

    public AudioClip trackIndex(int index) {
        switch(index){
            case 1:
            return desertTrack;

            case 2:
            return cityTrack;

            case 3:
            return waterTrack;

            case 4:
            return forestTrack;

            default: 
            return desertTrack;
        }
    }
    public AudioClip trackIndexBoss(int index) {
        switch(index){
            case 1:
            return desertBoss;

            case 2:
            return cityBoss;

            case 3:
            return waterBoss;

            case 4:
            return forestBoss;

            default: 
            return desertBoss;
        }
    }
}
