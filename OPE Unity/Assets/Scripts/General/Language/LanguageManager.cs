﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LanguageManager : MonoBehaviour
{
    public static Language currentLanguage;

    public static event Action OnLanguageChanged;

    public GameObject[] eng;
    public GameObject[] port;

    void Awake()
    {
        GetLanguage();

        if(currentLanguage == Language.Portuguese){
            for (int i = 0; i < port.Length; i++)
            {
                port[i].SetActive(true);
            }
            for (int i = 0; i < eng.Length; i++)
            {
                eng[i].SetActive(false);
            }
        }
        else {
            for (int i = 0; i < port.Length; i++)
            {
                port[i].SetActive(false);
            }
            for (int i = 0; i < eng.Length; i++)
            {
                eng[i].SetActive(true);
            }
        }
    }

    public void ChangeLanguage(Language newLang){
        //muda a lingua aqui e no SO
        currentLanguage = newLang;
        PlayerPrefs.SetString("CurrentLanguage", newLang.ToString());

        //muda a lingua de todos os textos com o componente TranslatbleText
        TranslatableText[] allTexts = FindObjectsOfType<TranslatableText>();
        for(int i = 0; i < allTexts.Length; i++){
            allTexts[i].UpdateLanguage(newLang);
        }

        OnLanguageChanged?.Invoke();
    }

    void GetLanguage(){
        string langTxt = PlayerPrefs.GetString("CurrentLanguage", "Portuguese");
        switch(langTxt){
            case "Portuguese": currentLanguage = Language.Portuguese;
            break;
            
            //ingles
            default: currentLanguage = Language.English;
            break;
        }
    }

    public void SwitchLanguage(){
        if(currentLanguage == Language.Portuguese){
            currentLanguage = Language.English;
        }
        else currentLanguage = Language.Portuguese;

        ChangeLanguage(currentLanguage);
    }

    public void SetEnglish(){
        ChangeLanguage(Language.English);
    }

    public void SetPortuguese(){
        ChangeLanguage(Language.Portuguese);
    }
}
[SerializeField]
public enum Language{
    Portuguese,
    English
}