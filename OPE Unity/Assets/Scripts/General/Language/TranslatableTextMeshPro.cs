﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TranslatableTextMeshPro : MonoBehaviour
{
    TextMeshPro textMeshPro;
    TextMeshProUGUI textMeshProUGUI;

    public string portuguese;
    public string english;

    void OnEnable()
    {
        UpdateLanguage(LanguageManager.currentLanguage);
    }

    void Start(){
        Invoke("UL", 0.1f); //pra ter certeza q foi certo
    }

    void UL(){
        UpdateLanguage(LanguageManager.currentLanguage);    
    }

    public void UpdateLanguage(Language newLanguage){
        string text = "";
        if(portuguese != "" || english != ""){
            switch(newLanguage){
                case Language.Portuguese: text = portuguese;
                break;

                default: text = english;
                break;
            }
        }

        //tmpro
        textMeshPro = GetComponent<TextMeshPro>();
        if(textMeshPro != null) textMeshPro.SetText(text);

        //ugui
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();           
        if(textMeshProUGUI != null) textMeshProUGUI.SetText(text);
    }

    public void SetText(string eng, string port){
        english = eng;
        portuguese = port;

        //atualiza o texto
        UL();
    }

    public void SetText(string s){ //muda os dois pra mesma coisa, caso o outro script pegue automatico de acordo com a lingua
        english =s;
        portuguese = s;

        //atualiza o texto
        UL();
    }
    
}
