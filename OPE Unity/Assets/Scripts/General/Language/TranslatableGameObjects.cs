﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslatableGameObjects : MonoBehaviour
{
    public bool portuguese;
    public bool english;

    public GameObject portugueseObj, englishObj;

    void OnEnable()
    {
        UpdateLanguage(LanguageManager.currentLanguage);
    }

    void Start(){
        UpdateLanguage(LanguageManager.currentLanguage);        
    }

    public void UpdateLanguage(Language newLanguage){
        switch(newLanguage){
            case Language.Portuguese: 
                portugueseObj.SetActive(true);
                englishObj.SetActive(false);
            break;

            default: 
                portugueseObj.SetActive(false);
                englishObj.SetActive(true);
            break;
        }
    }
}
