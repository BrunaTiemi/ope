﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new LanguageSO", menuName = "OPE Unity/LanguageSO")]
public class LanguageScriptableObject : ScriptableObject {
    public Language currentLanguage;
}

[System.Serializable]
public class LanguageText{
    public Language language;
    public string text;
}
