﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslatableTextMesh : MonoBehaviour
{
    public string portuguese;
    public string english;

    void OnEnable()
    {
        UpdateLanguage(LanguageManager.currentLanguage);
    }

    void Start(){
        UpdateLanguage(LanguageManager.currentLanguage);        
    }

    public void UpdateLanguage(Language newLanguage){
        if(portuguese != "" || english != ""){
            switch(newLanguage){
                case Language.Portuguese: GetComponent<TextMesh>().text = portuguese;
                break;

                default: GetComponent<TextMesh>().text = english;
                break;
            }
        }
    }
}
