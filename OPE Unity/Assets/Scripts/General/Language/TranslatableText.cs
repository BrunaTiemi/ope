﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteAlways]
public class TranslatableText : MonoBehaviour
{
    public string portuguese;
    public string english;

    void OnEnable()
    {
        if(GetComponent<Text>() == null){ //se nao tem um componente de texto
            if(GetComponent<TextMeshProUGUI>() != null) {//se tem um tmpro ugui
                TranslatableTextMeshPro ugui = this.gameObject.AddComponent<TranslatableTextMeshPro>(); //adiciona o componente pra ele
                ugui.SetText(english, portuguese); // atualiza o texto
                DestroyImmediate(this); // destroi esse componente
            }
        }
        else UpdateLanguage(LanguageManager.currentLanguage);
    }

    void Start(){
        if(GetComponent<Text>() != null) UpdateLanguage(LanguageManager.currentLanguage);        
    }

    public void UpdateLanguage(Language newLanguage){
        if(portuguese != "" || english != ""){
            switch(newLanguage){
                case Language.Portuguese: GetComponent<Text>().text = portuguese;
                break;

                default: GetComponent<Text>().text = english;
                break;
            }
        }
    }
}
