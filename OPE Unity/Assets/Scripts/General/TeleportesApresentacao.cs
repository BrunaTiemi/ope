﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportesApresentacao : MonoBehaviour
{
    public Transform[] teleportes;
    int currentTP;

    private void Start() {
        currentTP = -1;
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Period)){
            if(currentTP + 1 < teleportes.Length){
                currentTP++;
                Teleport();
            }
            else currentTP = teleportes.Length -1;
            
        }

        else if(Input.GetKeyDown(KeyCode.Comma)){
            if(currentTP - 1 >= 0){
                currentTP--;
                Teleport();
            }
            else currentTP = 0;
            
        }
    }

    void Teleport(){
        PlayerMovement._transform.position = new Vector3 (teleportes[currentTP].position.x, teleportes[currentTP].position.y, 0f);
    }
}
