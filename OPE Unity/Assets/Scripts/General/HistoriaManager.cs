﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HistoriaManager : MonoBehaviour
{
    public string sceneToGo;
    AsyncOperation asyncLoad;

    void Start(){
        StartCoroutine(LoadGame());
    }

    IEnumerator LoadGame(){
        asyncLoad = SceneManager.LoadSceneAsync(sceneToGo);
        asyncLoad.allowSceneActivation = false;

        yield return asyncLoad;
    }

    public void pressedButton(){
        asyncLoad.allowSceneActivation = true;
    }

}
