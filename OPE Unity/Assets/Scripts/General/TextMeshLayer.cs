﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMeshLayer : MonoBehaviour
{
    public MeshRenderer text;
    public SpriteRenderer fundo;

    void Start(){
        text.sortingOrder = 10;
        fundo.sortingOrder = 9;
    }
}
