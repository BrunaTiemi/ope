﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    bool invoked;
    public float delay;
    public GameObject cannonBall;

    public bool facingLeft;
    //ativa quando entra no collider SpawnArea
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "SpawnArea")
            if(invoked == false){
                invoked = true;
                InvokeRepeating("Shoot", 0f, delay);
            }

        //desativa se o player vai pra longe
        if(other.gameObject.tag == "DeactivateArea"){
            CancelInvoke();
            invoked = false;
        }
    }

    //se esta colidindo com o player e esta desativado, ativa
    private void OnTriggerStay2D(Collider2D other) {
        if(invoked == false){
            if(other.gameObject.tag == "SpawnArea"){
                invoked = true;
                InvokeRepeating("Shoot", 0f, delay);
            }
        }
    }

    void Shoot(){
        GameObject tempBullet = Instantiate(cannonBall, transform.position, Quaternion.identity);
        Rigidbody2D ballRB = tempBullet.gameObject.GetComponent<Rigidbody2D>();

        if(facingLeft) ballRB.velocity += Vector2.left * 8;

        else ballRB.velocity += Vector2.right * 8;
    }
}
