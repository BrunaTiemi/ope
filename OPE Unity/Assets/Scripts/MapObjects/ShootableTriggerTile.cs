﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootableTriggerTile : MonoBehaviour
{
    public bool activated, delay; //delay = tempo ate a pessoa poder ativar ele de novo.
    private SpriteRenderer SRenderer;

    public Color deactivatedC, activatedC;

    public MoveWhenShotTile[] movetiles;

    [Header("Variations")]
    [Space]
    //para multiplos triggers ativados necessarios
    public bool multipleTriggers;
    private MultipleTriggersDoor multipleTriggersScript;
    
    [Space]
    public bool keepActivating;
    [SerializeField] private bool invoked;

    public bool alwaysMove;

    //Timer
    [Space]
    [Tooltip ("Se depois de ativado, espera um tempo e desativa de novo")]
    public bool timed;
    public float timeToDeactivate = 4;

    [Tooltip("Quantas vezes deve ser ativado pra ser desligado depois")]
    public int QuantidadeVezesDesativar = 7;

    private void Start() {
        SRenderer = GetComponent<SpriteRenderer>();
        multipleTriggersScript = GetComponentInParent<MultipleTriggersDoor>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(!delay){ 
            if(other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet")){
                if(keepActivating){
                    KeepActivating();
                }
                else
                    UpdateState();

                Delay();
            }

            if(other.gameObject.tag == "SpawnArea"){
                if(alwaysMove) StartCoroutine(AlwaysMoveIEnum());
            }
        }
        
    }

    void ActivateBlock(){
            for(int i = 0; i < movetiles.Length; i++){
                movetiles[i].Activate();
            }
            
            if(timed){
                StartDeacTimer();
            }

        else print("No block to activate! " + this.gameObject.name);
    }

    void UpdateState(){
            activated = !activated;
            //muda a cor dependendo do estado atual
            SRenderer.color = (activated ? activatedC : deactivatedC);

            //ativa o bloco
            if(!multipleTriggers)
                ActivateBlock();
            else multipleTriggersScript.TriggerActivated();
    }

    //delay para a pessoa nao spammar o botao ou ele nao ativar direito.
    void Delay(){
        StartCoroutine(DelayCoroutine());
    }

    void StartDeacTimer(){
        StartCoroutine(TimerToDeactivate());
    }

    IEnumerator DelayCoroutine(){
        delay = true;

        yield return new WaitForSeconds(0.3f);
        
        delay = false;
    }

    IEnumerator TimerToDeactivate(){
        yield return new WaitForSeconds(timeToDeactivate);

        //se esta ativado, desativa
        if(activated){
            UpdateState();
        }
        CancelInvoke();
        invoked = false;
    }

    void KeepActivating(){
        StartCoroutine(KeepActivatingIEnum());
    }

    IEnumerator KeepActivatingIEnum(){
        for (int i = 0; i < QuantidadeVezesDesativar; i++){                
            UpdateState();
            invoked = true;

            yield return new WaitForSeconds(2f);
            
            //se esta ativado, desativa
            if(activated){
                UpdateState();
            }
        }
    }

    IEnumerator AlwaysMoveIEnum(){
        UpdateState();
        invoked = true;

        yield return new WaitForSeconds(2f);
        
        //se esta ativado, desativa
        StartCoroutine(AlwaysMoveIEnum());
    }
}
