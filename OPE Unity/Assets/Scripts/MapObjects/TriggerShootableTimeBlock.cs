﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerShootableTimeBlock : MonoBehaviour
{
    public ActivatedTimedBlock tileScript;
    public SpriteRenderer SRenderer;
    public Color deactivatedC, activatedC;
    public bool active;
    private bool delay; //para nao ativar varias vezes sem querer

    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet")){
            if(!delay){
                //se possui multiplos triggers
                if(tileScript.multipleTriggers){
                    active = !active;
                    SRenderer.color = (active ? activatedC : deactivatedC);
                    tileScript.CheckTriggers();
                }//se nao
                else
                    tileScript.activate();
                
                StartCoroutine(DelayCoroutine());
            }
        }
    }

    public void Desativar(){
        SRenderer.color = deactivatedC;
        active = false;
    }

    IEnumerator DelayCoroutine(){
        if(delay == false){
        delay = true;

        yield return new WaitForSeconds(0.3f);

        delay = false;
        }
    }
}
