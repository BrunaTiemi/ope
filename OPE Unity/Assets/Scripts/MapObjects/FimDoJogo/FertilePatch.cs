﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FertilePatch : MonoBehaviour
{
    private FlowerScript flowerScript;
    private PlayerMovement playerMovement;
    public Transform midPointTransform; //a flor vai pra cima da terra, e depois pra baixo pra ser enterrada. Esse eh o ponto de cima.

    public GameObject particulas;

    public Transform endPos;

    bool animating; //pra so tocar uma vez

    private void OnTriggerEnter2D(Collider2D other) {
        if(!animating){
            if(other.gameObject.tag == "Player"){
                flowerScript = GameObject.FindObjectOfType<FlowerScript>();
                playerMovement = other.GetComponentInParent<PlayerMovement>();

                Animacao();
                animating = true;
            }
        }
    }

    void Animacao(){
        StartCoroutine(AnimaCoroutine());
    }
    IEnumerator AnimaCoroutine(){
        //vai pro ponto acima da terra
        flowerScript.FoundFertilePatch(midPointTransform);
        playerMovement.Walk(true); //player anda

        yield return new WaitForSeconds (1.5f);
        
        //vai pra dentro da terra
        flowerScript.FoundFertilePatch(endPos);

        
        yield return new WaitForSeconds(1.9f);

        //atuva as particulas
        particulas.SetActive(true);

        yield return new WaitForSeconds(0.6f);
        
        playerMovement.Walk(false); //player corre
        
        
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Creditos");
        asyncLoad.allowSceneActivation = false;

        yield return new WaitForSeconds(5.5f);

        PlayerPrefs.SetInt("AcabouJogo", 1);
        asyncLoad.allowSceneActivation = true;

    }
}
