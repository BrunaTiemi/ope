﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformEffectorHandler : MonoBehaviour
{

    public PlatformEffector2D platform;

    private bool playerNear;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            playerNear = true;
        }
    }

    private void Update() {
        if(playerNear){
            if(Input.GetKeyDown(KeyCode.S)){
                platform.rotationalOffset = 180f;
                Invoke("ResetRotation", 0.2f);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            playerNear = false;
        }
    }
    private void ResetRotation(){
        platform.rotationalOffset = 0f;
    }
}
