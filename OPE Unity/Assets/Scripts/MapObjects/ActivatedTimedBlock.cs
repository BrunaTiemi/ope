﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatedTimedBlock : MonoBehaviour
{
    public GameObject[] blocos;
    public float tempo;

    public bool multipleTriggers;
    public TriggerShootableTimeBlock[] triggers;

    public bool switchInsteadOfActivating;

    public void activate(){
        for(int i=0;i<blocos.Length;i++){
            blocos[i].SetActive(true);
        }
        Invoke("deactivate",tempo);
    }
    public void deactivate(){
        DeactivateBlocks();
        for(int i=0;i<triggers.Length;i++){
            triggers[i].Desativar();
        }
    }
    public void DeactivateBlocks(){
        for(int i=0;i<blocos.Length;i++){
            blocos[i].SetActive(false);
        }
    }
    public void Switch(){
        bool active = false;
        for(int i=0;i<blocos.Length;i++){
            active = blocos[i].activeSelf;
            blocos[i].SetActive(!active);
        }
    }

    public void CheckTriggers(){
        if(AllTriggered()){
            if(switchInsteadOfActivating){
                Switch();
            }
            else
                activate();
            Debug.Log("all triggered");
        }
        else {
            DeactivateBlocks();
        }
    }

    public bool AllTriggered(){
        for(int i = 0; i < triggers.Length; i++){
            if(triggers[i].active == false){
                return false;
            }
        }
        return true;
    }
}
