﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorFollowOtherObj : MonoBehaviour
{
    public Transform follow;

    public bool followRotation, followScale;

    bool cantFollow;

    private void Start() {
        //para nao seguir no play
        if(Application.isPlaying){
            cantFollow = true;
        }
    }

    void Update()
    {
        if(!cantFollow && follow != null){
            this.transform.position = follow.transform.position;

            if(followRotation){
                this.transform.rotation = follow.transform.rotation;
            }

            if(followScale){
                this.transform.localScale = follow.transform.localScale;
            }
        }
    }
}
