using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructOnTriggerEnter : MonoBehaviour
{
    public string[] tags;
    public float delay;
    private float onlyDestroyAfter;

    //Se destroi quando encosta em alguma das tags
    private void OnTriggerEnter2D(Collider2D other) {
        if(Time.time > onlyDestroyAfter)
            for(int i = 0; i < tags.Length; i++)
                if(other.gameObject.tag == tags[i]){
                    Destroy(this.gameObject, 0.1f);
                }
    }

    void Start(){
        onlyDestroyAfter = Time.time + delay;
        Destroy(this.gameObject, 3f);
    }
    
}
