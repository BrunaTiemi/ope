﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayModeDisableComponent : MonoBehaviour
{
    public SpriteRenderer[] spriteRenderer;

    void Start()
    {
        if(spriteRenderer.Length > 0){
            for (int i = 0; i < spriteRenderer.Length; i++)
            {
                spriteRenderer[i].enabled = false;
            }
        }
    }
}
