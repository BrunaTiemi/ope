﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GetAbilityTile : MonoBehaviour
{
    public static event Action<Ability> EnableAbility;
    bool sentEvent;
    public GameObject Instrucao;
    public AbilitiesSO abilitiesSO;

    public Ability abilityToGive;
    private void Start() {        
        //se esta == parry, verifica se eh o parry mesmo e pega a habilidade certa dependendo do SO
        if(abilityToGive == Ability.Parry){
            abilityToGive = CheckAbilityToGive();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            Instrucao.SetActive(true);            
            Instrucao.GetComponentInChildren<TranslatableTextMeshPro>().SetText("Press 'F' to receive a new Ability!", "Aperte 'F' para receber uma Habilidade Nova!");
            KeyboardEvents.OnPressedF += GiveAbility; //se inscreve no evento de dar habilidade
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            Instrucao.SetActive(false);
            KeyboardEvents.OnPressedF -= GiveAbility;
        }
    }

    //quando o player apertar F, recebe a habilidade
    void GiveAbility(){
        if(!sentEvent){ // p nao enviar 2 vezes
            sentEvent = true;
            EnableAbility(abilityToGive);
        }

        //instancia o texto novo para ele persistir enquanto esse objeto é deletado
        Instantiate(Instrucao, transform.position + Vector3.up * 2f, Quaternion.identity).GetComponentInChildren<TranslatableTextMeshPro>().SetText(GetText());
        //abrir inv
        Instantiate(Instrucao, transform.position + Vector3.up * 1f, Quaternion.identity).GetComponentInChildren<TranslatableTextMeshPro>().SetText(abilitiesSO.GetOpenInvText());
        
        Destroy(this.gameObject);
    }

    string GetText(){
        return abilitiesSO.GetKeyToUseText(abilityToGive);
    }

    public Ability CheckAbilityToGive(){
        int nivel = GameManager.nivelAtual;
        int fase = GameManager.faseAtual;

        return abilitiesSO.GetAbilityByStage(nivel, fase);

    }
}