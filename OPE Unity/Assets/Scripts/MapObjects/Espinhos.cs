﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espinhos : MonoBehaviour
{
    public float delayAtivar = 0.3f, tempoDesativar = 1f;
    bool active;
    public Animator anim;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            if(!active){
                StartCoroutine(Delay());
            }
        }
    }

    IEnumerator Delay(){
        //espera um tempo pra ativar
        yield return new WaitForSeconds(delayAtivar);
        active = true;
        MoveSpikes();

        yield return new WaitForSeconds(tempoDesativar);
        active = false;
        MoveSpikes();
    }

    void MoveSpikes(){
        if(active){
            anim.Play("moveUp");
        }
        else anim.Play("moveDown");
    }
}
