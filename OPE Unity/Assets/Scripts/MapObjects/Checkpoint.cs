﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Checkpoint : MonoBehaviour
{
   public Animator anim;
   public int id; //o id comeca por 1. se estiver no save como 0 eh pq nao pegou nenhum checkpoint.
   public bool isActive;
   public static event Action DeactivateAll;
   public static event Action<int> ActivatedCheckpoint;

    private void OnEnable() {
        Checkpoint.DeactivateAll += SetInactive;
    }
    private void OnDisable() {
        Checkpoint.DeactivateAll -= SetInactive;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        //se o player encostou no checkpoint
        if(other.gameObject.tag == "Player"){
            anim.SetBool("activate", true);
            //desativa todos os checkpoints e se ativa
            DeactivateAll();
            isActive = true;

            //salva que o checkpoint foi pego
            if(ActivatedCheckpoint != null) ActivatedCheckpoint(id);
        }
       // print(other.gameObject.tag);
    }

    void SetInactive(){
        isActive = false;
    }


}
