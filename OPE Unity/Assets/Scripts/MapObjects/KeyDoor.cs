﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    public GameObject noKeyInstruction;
    public GameObject PressKeyInstruction;

    public TranslatableTextMeshPro noKeyText;
    Inventory inv;

    public int keysNeeded = 1; //nao pode ser 0

    bool delay;

    bool hasKey => inv.keys >= keysNeeded;

    private void OnTriggerEnter2D(Collider2D other) {
        if(keysNeeded == 0) keysNeeded = 1; //nao pode ser 0
        if(other.gameObject.tag == "Player"){
            inv = other.GetComponentInChildren<Inventory>();

            if(hasKey){
                PressKeyInstruction.SetActive(true);
            }
            else {
                NoKeyString();
                noKeyInstruction.SetActive(true);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            if(Input.GetKey(KeyCode.F)){
                Usekey();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            //desativa as instrucoes
            noKeyInstruction.SetActive(false);
            PressKeyInstruction.SetActive(false);
        }
    }

    private void Usekey(){
            if(hasKey && !delay){
                inv.RemoveItem(Item.key);
                Destroy(this.gameObject);
                Delay();
            }
    }

    private void Delay(){
        StartCoroutine(DelayCoroutine());
    }

    IEnumerator DelayCoroutine(){
        delay = true;

        yield return new WaitForSeconds(0.1f);

        delay = false;        
    }

    void NoKeyString(){
        /*string texto = "";
        switch(LanguageManager.currentLanguage){
            case Language.Portuguese: 
                texto = $"Você precisa de {keysNeeded} chave";
                break;

            case Language.English:
                texto = $"You need {keysNeeded} key";
                break;
        }

        if(keysNeeded != 1) texto += "s";
        return texto;*/

        noKeyText.SetText($"You need {keysNeeded} key", $"Você precisa de {keysNeeded} chave");
    }
}
