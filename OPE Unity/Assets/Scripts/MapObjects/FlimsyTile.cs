﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlimsyTile : MonoBehaviour
{
    private Rigidbody2D rb;
    public float timeToFall = 0.3f;
    public float fallSpeed = 3f;

    Vector3 posInicial;

    public PolygonCollider2D poliCollider;

    public bool testesFallNow;

    private void OnEnable() {
        posInicial = transform.position;
    }
    private void Update() {
        if(testesFallNow){
            testesFallNow = false;
            rb = gameObject.GetComponent<Rigidbody2D>();
            StartCoroutine(Break());
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            rb = gameObject.GetComponent<Rigidbody2D>();
            StartCoroutine(Break());
        }
    }
    IEnumerator Break(){
        //cai
        yield return new WaitForSeconds(timeToFall);
        rb.velocity = Vector2.down * fallSpeed;
        poliCollider.enabled = true;

        //volta pra pos inicial
        yield return new WaitForSeconds(3f);
        transform.position = posInicial;
        rb.velocity = new Vector2(0f, 0f);
        poliCollider.enabled = false;
    }
}
