﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleTriggersDoor : MonoBehaviour
{
    public ShootableTriggerTile[] tiles;
    public MoveWhenShotTile[] moveTile;
    //OPEN E CLOSE TAO TROCADOS

    //checa se todos estão ativos. Ativado sempre que um dos tiles é ativado.
    public void TriggerActivated(){
        if(CheckAllActivated()){
            foreach(MoveWhenShotTile move in moveTile){
                move.Close();
                print("move");
            }
        }
        else foreach(MoveWhenShotTile move in moveTile){
            move.Open();
        }
    }
    public bool CheckAllActivated(){
        for(int i = 0; i < tiles.Length; i++){
            if(tiles[i].activated == false){
                return false;
            }
            else print("Activated " + i);
        }
        return true;
    }
}
