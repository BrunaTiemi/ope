﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicSmoke : MonoBehaviour
{
    //public GameObject smoke;
    public ParticleSystem particulas;
    public GameObject child;
    public float delay;
    public bool invoked, active;

    PlayerScript player;
    Animator animator;
    
    //ativa quando entra no collider SpawnArea
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "SpawnArea")
            if(invoked == false){
                invoked = true;
                child.SetActive(true);
                InvokeRepeating("Smoke", 0f, delay);
            }

        //desativa se o player vai pra longe
        if(other.gameObject.tag == "DeactivateArea"){
            CancelInvoke();
            invoked = false;
            active = false;
        }
    }

    //se esta colidindo com o player e esta desativado, ativa
    private void OnTriggerStay2D(Collider2D other) {
        if(invoked == false){
            if(other.gameObject.tag == "SpawnArea"){
                invoked = true;
                InvokeRepeating("Smoke", 0f, delay);
            }
        }
    }

    private void OnParticleCollision(GameObject other) {
        if(other.gameObject.tag == "Player"){
            if(player == null) player = other.GetComponentInChildren<PlayerScript>();
            player.LevaDano(1);

            print("colidiuP");
        }
            print("colidiuoutro");
    }

    void Smoke(){
        particulas.Play();

        if(animator == null) animator = GetComponentInChildren<Animator>();
        animator.SetTrigger("move");
    }
    
}
