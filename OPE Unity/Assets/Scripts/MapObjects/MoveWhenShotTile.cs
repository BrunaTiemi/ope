﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWhenShotTile : MonoBehaviour
{
    public Transform pointA, pointB;
    private Vector3 endPos;
    [SerializeField] private bool move, towardsA, playerTouching;
    [SerializeField] private int speed = 3;
    [SerializeField] [Header("Se carrega o player junto")] bool platform;
    
    public bool alwaysMove;
    public bool naoPrecisaDoPlayerPerto;
    public bool moveCimaBaixo;
    bool insideSpawnArea;
  

    public bool currentA => Vector3.Distance(transform.position, pointA.position) < Vector3.Distance(transform.position, pointB.position);
    public bool currentB => Vector3.Distance(transform.position, pointB.position) < Vector3.Distance(transform.position, pointA.position);

    public bool reachedDestination => Vector3.Distance(transform.position, pointA.position) < 0.05 || Vector3.Distance(transform.position, pointB.position) < 0.05;

    Transform playerTransform => PlayerMovement._transform;
    private Vector3 nextPosition;

    void Move(){
        endPos = Vector3.Lerp(transform.position, nextPosition, speed * Time.fixedDeltaTime);
        transform.position = endPos;
        Vector3 playerEndPos = Vector3.zero;

        if(platform && playerTouching){
            if(moveCimaBaixo) playerEndPos = Vector3.Lerp(playerTransform.position, new Vector3(playerTransform.position.x, nextPosition.y, playerTransform.position.z), speed * Time.fixedDeltaTime);
            else playerEndPos = Vector3.Lerp(playerTransform.position, new Vector3(nextPosition.x, playerTransform.position.y, playerTransform.position.z), speed * Time.fixedDeltaTime);
            
            PlayerMovement._transform.position = playerEndPos; 
        }
    }

    private void FixedUpdate() {
        if(move){
            Move();
            CheckIfInPlace();
        }

        if((insideSpawnArea && alwaysMove)|| naoPrecisaDoPlayerPerto){
            if(move == false){
                Activate();
            }
        }
    }

    public void Activate(){
        //escolhe em qual direcao precisa ir
        if(currentA){
            nextPosition = pointB.position;
            towardsA = false;
        }
        else if(currentB) {
            nextPosition = pointA.position;
            towardsA = true;
        }

        move = true;
    }

    public void Close(){
        nextPosition = pointB.position;
        towardsA = false;

        move = true;
    }
    public void Open(){
        nextPosition = pointA.position;
        towardsA = true;

        move = true;
    }

    void CheckIfInPlace(){
        //se alcancou o destino, desliga o movimento.
        if(reachedDestination){
            move = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            playerTouching = true;
        }
        if(other.gameObject.tag == "SpawnArea"){
            insideSpawnArea = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            playerTouching = false;
        }
        if(other.gameObject.tag == "SpawnArea"){
            insideSpawnArea = false;
        }
    }
}
