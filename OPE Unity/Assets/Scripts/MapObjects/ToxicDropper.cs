﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicDropper : MonoBehaviour
{
    public GameObject drop;
    public RectTransform rt;
    public float height, minDiff, maxDiff;

    [Header("Menor, mais rapido")]
    public float dropRate = 0.5f;
    bool invoked;

    public bool testMaxMin;

    private void OnTriggerEnter2D(Collider2D other) {

#if !UNITY_EDITOR
    testMaxMin = false;
#endif

    if(rt == null) rt = GetComponent<RectTransform>();

        if(other.gameObject.tag == "SpawnArea")
            if(invoked == false){
                invoked = true;
                InvokeRepeating("ToxicDrop", 0f, dropRate);
                if(testMaxMin){
                    InvokeRepeating("DropMax", 0f, 0.1f);
                    InvokeRepeating("DropMin", 0f, 0.1f);
                }
            }
        if(other.gameObject.tag == "DeactivateArea"){
            CancelInvoke();
            invoked = false;
        }
    }

    //se esta colidindo com o player mas nao esta ativo, ativa
    private void OnTriggerStay2D(Collider2D other) {
        if(invoked == false){
            if(other.gameObject.tag == "SpawnArea"){
                invoked = true;
                InvokeRepeating("ToxicDrop", 0f, dropRate);
            }
        }
    }
    void ToxicDrop(){
        //pega algum ponto aleatorio de dentro do rect transform pra instanciar a gota
        Vector3 pos = new Vector3(Random.Range(rt.rect.xMin + minDiff + transform.position.x, rt.rect.xMax + transform.position.x + maxDiff), transform.position.y + height, 0f);
        Transform go = Instantiate(drop, pos, Quaternion.identity).GetComponent<Transform>();
        go.SetParent(this.transform);
    }

    void DropMax(){
        //pega algum ponto aleatorio de dentro do rect transform pra instanciar a gota
        Vector3 pos = new Vector3(rt.rect.xMax + transform.position.x + maxDiff, transform.position.y + height, 0f);
        Transform go = Instantiate(drop, pos, Quaternion.identity).GetComponent<Transform>();
        go.SetParent(this.transform);
    }

    void DropMin(){
        //pega algum ponto aleatorio de dentro do rect transform pra instanciar a gota
        Vector3 pos = new Vector3(rt.rect.xMin + minDiff + transform.position.x, transform.position.y + height, 0f);
        Transform go = Instantiate(drop, pos, Quaternion.identity).GetComponent<Transform>();
        go.SetParent(this.transform);
    }
    
}
