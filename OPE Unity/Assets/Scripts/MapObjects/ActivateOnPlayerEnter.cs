﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnPlayerEnter : MonoBehaviour
{
    public GameObject[] activate;
    public GameObject[] deactivate;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            for (int i = 0; i < activate.Length; i++)
            {
                activate[i].SetActive(true);
            }
            for (int i = 0; i < deactivate.Length; i++)
            {
                deactivate[i].SetActive(false);
            }
        }
    }
}
