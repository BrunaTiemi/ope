﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static event Action OnSceneChanged;
    public int nivel;
    public int fase;

    public static int nivelAtual;
    public static int faseAtual;

    public LetterText letterText;
    public PlayerScript playerScript;

    private SaveGame save;
    public InventoryMenu inventory;

    private enum PP { //nomes dos playerprefs para nao digitar errado ou esquecer
        LastLevelPlayed, 
        LastStageHealth
    };

    AsyncOperation carregarCena;
    public TransicaoScript transicaoScript;
    private bool isLoadingScene;
    public static event Action MesmoNivel;

    [Header("Checkpoints")]
    public int lastCheckpoint;
    public Checkpoint[] checkpoints; //todos os checkpoints da fase
    bool salvouFinalDoNivel; // para nao ter repeticao na hora de salvar.

    public bool mesmoNivel;
    public bool mesmaFase;

    [SerializeField] int InimigosMortos;

    public static int inimigosMortos; //conta os inimigos mortos na fase

    //Testing
    [Header("Testes")]
    public bool testando;
    private bool mainCameraIsOn = true;
    public static bool testes;
    public GameObject testesTextos;

    //Menus
    public GameObject pauseMenu;
    private AnimationTransitions pauseAnimTransitions;
    public bool pause;

    //Cursor
    public Texture2D miraImg;

    [Header ("Cameras")]
    public Camera mainCamera;
    public Camera testsCamera;
    
    #region StartUp

    void Awake(){
        FindNullObjects();
        PegaNivelFase();

        nivelAtual = nivel;
        faseAtual = fase;
    }

    private void OnEnable() {
        Checkpoint.ActivatedCheckpoint += ActivatedCheckpoint; //evento para salvar o checkpoint pego atualmente
        //SceneManager.activeSceneChanged += ChangedScene;
    }
    private void OnDisable() {
        Checkpoint.ActivatedCheckpoint -= ActivatedCheckpoint;
        //SceneManager.activeSceneChanged -= ChangedScene;
    }

    void Start(){
        //desabilita testes fora do editor. pode ser ativado de novo com P
        #if !UNITY_EDITOR
    
        testando = false;
   
        #endif
        inimigosMortos = 0;
        
        if(PlayerPrefs.GetInt("previouStage") == fase){
            mesmaFase = true;
            print("mesma fase " + PlayerPrefs.GetInt("previouStage"));
        }
        else 
            print("não mesma fase");

        if(PlayerPrefs.GetInt("previousLevel") == nivel) mesmoNivel = true;

        //checa se o pause esta ativado e desativa
        if(pauseMenu.activeSelf){
            pauseMenu.SetActive(false);
        }

        //set cursor
        Cursor.SetCursor(miraImg, new Vector2(miraImg.width / 2, miraImg.height / 2), CursorMode.Auto);
        
        if(mesmoNivel){
            Invoke("EventoMesmoNivel", 0.1f);
        }
        
        GetCheckpoints(); //pega os checkpoints e teleporta o player para o ultimo checkpoint

        Time.timeScale = 1;
        
        SaveData(); //salva o nivel e a fase atuais
        print("Game Started");

        //Ativa o evento que mudou de cena (para mudar a musica)
        if(OnSceneChanged != null){
            OnSceneChanged();
        }
    }
    void PegaNivelFase(){
        string currentScene = SceneManager.GetActiveScene().name;

        if(currentScene.Contains("Boss")) fase = 4;
        else {
            try {fase = (int)Char.GetNumericValue(currentScene[10]);
            }
            catch { fase = 1;}
        }

        try {nivel = (int)Char.GetNumericValue(currentScene[5]);}
        catch {nivel = 1;}

    }

    void FindNullObjects(){
        if(pauseMenu == null){
            pauseMenu = GameObject.Find("PauseMenu");
        }
        if(testesTextos == null){
            pauseMenu = GameObject.Find("TestesTextos");
        }
        if(playerScript == null){
            playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
        }

        save = GetComponent<SaveGame>();

        pauseAnimTransitions = pauseMenu.GetComponent<AnimationTransitions>();
    }

    void EventoMesmoNivel(){
        MesmoNivel();
    }
    GameObject GetPlayer(){
        GameObject player = GameObject.Find("PlayerPrefab");
        return player;
    }

    //Checkpoints
    void GetCheckpoints(){
        checkpoints = UnityEngine.Object.FindObjectsOfType<Checkpoint>().OrderBy(x => x.transform.GetSiblingIndex()).ToArray();
        for (int i = 0; i < checkpoints.Length; i++)
        {
            checkpoints[i].id = i+1; //o id comeca por 1. se estiver no save como 0 eh pq nao pegou nenhum checkpoint.
        }

        if(!mesmaFase){ //se nao esta na mesma fase, reseta os checkpoints
            ResetCheckpoints();
            Debug.Log("Resetou os checkpoints");
        } else {
            //pega o ultimo checkpoint do save
            lastCheckpoint = save.gameData.lastCheckpoint;
            if(lastCheckpoint > 0 && checkpoints.Length >= lastCheckpoint){
                int id = lastCheckpoint-1;
                Transform checkpointT = checkpoints[id].transform;
                playerScript.TeleportPlayer(checkpointT.position.x, checkpointT.position.y);
            }
        }
    }

    void ResetCheckpoints(){
        //save.gameData.lastCheckpoint = 0;
    }

    #endregion

    private void OnApplicationQuit() {
        save.gameData.lastCheckpoint = 3;
        SaveData();
    }
    
    void ActivatedCheckpoint(int checkpointID){ //muda a variavel para ser salvo depois
        lastCheckpoint = checkpointID;
        SaveData();
        PlayerPrefs.SetInt("previouStage", fase);
        print("ativou checkpoint" + checkpointID);
    }

    void Update(){

        if(Input.GetKeyDown(KeyCode.Escape)){
            //se o inventario estiver aberto, fecha
            if(inventory.isOpen){
                inventory.CloseInventory();
            }
            else {//se nao, despausa o jogo
                TogglePauseMenu();
            }
        }


        ///TESTES

        //ativa modo testes
        if(Input.GetKeyDown(KeyCode.ScrollLock)){
            testando = !testando;
        }

        if(testando) {
            testes = true;
            Testes();
        }
        else {
            if(testes == true){ //se estava testando e nao esta mais
                testesTextos.SetActive(false);
                
                mainCamera.enabled = true;
                testsCamera.enabled = false;
            }
            testes = false;
        }

    }

    bool canOpenPauseBeginning;
    public void TogglePause(){
        if(!canOpenPauseBeginning){
            if(GameObject.FindObjectOfType<ShowStageScreen>() != null) return;
            else canOpenPauseBeginning = true;
        }

        //pausa
        if(pause == false){
            Pause();
        }
        //despausa
        else{
            Unpause();
        }
    }
    public void TogglePauseMenu(){
        //pausa
        if(pause == false){
            if(!canOpenPauseBeginning){//procura o objeto que msotra a cena pra n ficar preso la caso a pessoa tente abrir o pause cedo
                if(GameObject.FindObjectOfType<ShowStageScreen>() != null) return;
                else canOpenPauseBeginning = true;
            }
            Pause();
            pauseMenu.SetActive(true);
        }
        //despausa
        else{
            Unpause();
            pauseMenu.GetComponent<Animator>().SetTrigger("disable");
        }
    }


    public void Pause(){
        pause = true;
        Time.timeScale = 0;
    }

    public void Unpause(){        
        pause = false;
        Time.timeScale = 1;    
        pauseMenu.SetActive(false);   
        //pauseAnimTransitions.BackToStageOne();
    }


    public void EndStageWithDelay(float delay){
        Invoke("EndStage", delay);
    }
    public void EndStage(){
        if(!isLoadingScene){
            PlayerPrefs.SetInt("previouStage", fase);
            PlayerPrefs.SetInt("previousLevel", nivel);
            
            string nextScene = GetNextStage();
            Debug.Log("Loading " + nextScene);
            StartCoroutine(LoadAsyncOperation(nextScene));

            if( !salvouFinalDoNivel ){
                save.gameData.AddDeadEnemies(inimigosMortos);
                SaveData();
                salvouFinalDoNivel = true;
            }
            print("salvou fase " + fase);
        }
    }

    public void SaveData(){
        save.gameData.nivel = nivel;
        save.gameData.fase = fase;
        save.gameData.lastCheckpoint = lastCheckpoint;
        print("salvou" + lastCheckpoint);
        save.SaveData(this.gameObject.name);
    }

    string GetNextStage(){
        if(SceneManager.GetActiveScene().name == "nivel3BossFight") return "FinalDoJogo";

        if(fase == 3){
            return "nivel" + nivel + "BossFight";
        }
        else if(fase == 4) return "nivel" + (nivel + 1) + "fase" + 1;
        else return "nivel" + nivel + "fase" + (fase +1);
    }
    IEnumerator LoadAsyncOperation(string cena){ //operacao assincrona para carregar a proxima cena
        isLoadingScene = true;
        TransicaoScript.OnTransitionFinish += ChangeSceneNow;
        carregarCena = SceneManager.LoadSceneAsync(cena);
        carregarCena.allowSceneActivation = false;

        transicaoScript.FadeIn();

        yield return new WaitForSeconds(4.6f);
        carregarCena.allowSceneActivation = true;
    }

    void ChangeSceneNow(){
        carregarCena.allowSceneActivation = true;
    }

    void Testes(){

        //Restart Level
        if(Input.GetKeyDown(KeyCode.R)){
            SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex);
        }

        //Teleports player to mouse pos
        if(Input.GetKeyDown(KeyCode.T)){
            GameObject player = GetPlayer();

            //Get mouse position and teleports the player to it
            Vector3 mousePos = testsCamera.ScreenToWorldPoint(Input.mousePosition);
            if(!mainCameraIsOn) player.transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
            else player.transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
        }
        //Show Map
        if(Input.GetKeyDown(KeyCode.M)){
            mainCameraIsOn = !mainCameraIsOn;

            if(mainCameraIsOn){
                mainCamera.enabled = true;
                testsCamera.enabled = false;
            }
            else{
                mainCamera.enabled = false;
                testsCamera.enabled = true;
            }
        }

        //testesTextos.SetActive(true);
    }
    
}
public enum Tags {
        Player,
        GameController,
        BulletPlayer,
        Enemy,
        Espada,
        SpawnArea,
        DirectionCollider,
        Checkpoint,
        EnemyBullet,
        DamagePlayer,
        Tilemap,
        DestroyOnShoot,
        Trap,
        ItemVida,
        ItemShield
}
