﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class ChangeFOV : MonoBehaviour
{
    public PixelPerfectCamera pixelPerfectCamera;

    public void ChangeFov(float newFov){
        int f = (int)newFov;

        if(pixelPerfectCamera == null) pixelPerfectCamera = GameManager.FindObjectOfType<PixelPerfectCamera>();
        pixelPerfectCamera.assetsPPU = f;

        PlayerPrefs.SetInt("FOV", f); // eh pegado no script de seguir player
    }
}
