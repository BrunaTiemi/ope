﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Vector3 posInicial;
    public float xMax, yMax;
    public float xMin, yMin;

    public float shakeTime;
    public bool lessShakyShake;

    private CameraFollowPlayer cameraFollowPlayer;

    public void ShakeCamera(){
        //desliga o componente da camera seguir o player
        if(cameraFollowPlayer == null) cameraFollowPlayer = GetComponent<CameraFollowPlayer>();
        cameraFollowPlayer.enabled = false;

        if(!lessShakyShake) StartCoroutine(ShakeCoroutine());
        else StartCoroutine(LessShakeCoroutine());
    }

    public void LessShakyShake(){
        lessShakyShake = true;

        ShakeCamera();
    }

    private IEnumerator ShakeCoroutine(){
        posInicial = transform.position;
        
        InvokeRepeating("TeleportToRandomPos", 0f, 0.2f);

        yield return new WaitForSeconds(shakeTime);

        CancelInvoke(); //para de tremer

        //liga o componente da camera seguir o player
        cameraFollowPlayer.enabled = true;

        //desabilita o Less Shaky Shake
        lessShakyShake = false;
    }

    private IEnumerator LessShakeCoroutine(){
        posInicial = transform.position;
        
        InvokeRepeating("TeleportToRandomPos", 0f, 0.2f);

        //print("shakeTime" + shakeTime/4);
        yield return new WaitForSeconds(0.2f);

        CancelInvoke(); //para de tremer

        //liga o componente da camera seguir o player
        cameraFollowPlayer.enabled = true;

        //desabilita o Less Shaky Shake
        lessShakyShake = false;
    }

    private void TeleportToRandomPos(){        
        transform.position = GetRandomPos();
    }

    private Vector3 GetRandomPos(){        
        //soma a posicao do player com a diferenca daqui
        Vector3 newPos = cameraFollowPlayer.GetFollowPlayerPos() + new Vector3(Random.Range(xMin, xMax), Random.Range(yMin, yMax), transform.localPosition.z);
        
        if(lessShakyShake){
            newPos =  cameraFollowPlayer.GetFollowPlayerPos() + new Vector3(Random.Range(xMin *0.3f, xMax*0.3f), Random.Range(yMin*0.3f, yMax*0.3f), transform.localPosition.z);
        }

        return newPos;
    }
}
