﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class CameraFollowPlayer : MonoBehaviour
{
    PixelPerfectCamera pixelPerfectCamera;

    public PlayerScript player;
    public float diferenca, dify;
    private float distanceToPlayer;
    public bool startTurnedLeft;
    private bool dirHasChanged;
    
    Vector3 playerPos;
    Vector3 smoothPos;
    public float moveSpeed = 6;
    private float SavedMoveSpeed;
    
    //Lambdas
    private Vector3 diferencaCam => new Vector3(diferenca, dify, -243f);
    private Vector3 diferencaCamLeft => new Vector3(0f, dify, -243f);

    private Vector3 currentDirDifference; //que lado o player esta vidado atualmente

    void Start(){
        //pega o fov do save
        pixelPerfectCamera = GetComponent<PixelPerfectCamera>();
        pixelPerfectCamera.assetsPPU = PlayerPrefs.GetInt("FOV", 74);

        SavedMoveSpeed = moveSpeed; //salva o movespeed para pegar depois

        currentDirDifference = startTurnedLeft ? diferencaCamLeft : diferencaCam;

        //comeca ja na posicao certa
        transform.position = PlayerMovement._transform.position + currentDirDifference;
    }

    void FixedUpdate(){
        GetPlayerDir(); 

        playerPos = PlayerMovement._transform.position + currentDirDifference;

        //move o player
        smoothPos = Vector3.Lerp(transform.position, playerPos, 6 * Time.fixedDeltaTime);
        transform.position = smoothPos;
    }

    void GetPlayerDir(){
        //se player esta virado pra esquerda ou pra direita, usa os valores diferentes
        switch(player.movement.inputHorz){
            case 1:
                dirHasChanged = currentDirDifference != diferencaCam ? true : false; //verifica se o valor mudou
                currentDirDifference = diferencaCam;                                 //seta o valor novo
                break;

            case -1:
                dirHasChanged = currentDirDifference != diferencaCamLeft ? true : false; //verifica se o valor mudou
                currentDirDifference = diferencaCamLeft;                                 //seta o valor novo
                break;

            default: 
                break;
        }
        if(dirHasChanged){
            DecreaseSpeed();
        }

    }

    public Vector3 GetFollowPlayerPos(){        
        return PlayerMovement._transform.position + currentDirDifference;
    }
    
    void DecreaseSpeed(){
        moveSpeed = 1;
        Invoke("SetToDefaultSpeed", 1f);
    }
    void SetToDefaultSpeed(){
        moveSpeed = SavedMoveSpeed;
    }
}
