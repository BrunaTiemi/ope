﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjSave", menuName = "OPE/SaveScriptableObject")] 
public class SaveStateScriptableObject : ScriptableObject
{
    //
    ///Only saves during runtime, after restarting the game, the save is gone
    //
    public SavesSO[] obj;
}

[System.Serializable]
public class SavesSO {
    public string saveName;
    public int nivel, fase;
    public bool active;
    public float value;
}