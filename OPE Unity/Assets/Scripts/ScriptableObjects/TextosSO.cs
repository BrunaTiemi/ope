﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TextosSO", menuName = "OPE/TextosSO")]
public class TextosSO : ScriptableObject
{
    public Texto[] niveis;

    [Header("o que vai aparecer antes do numero da fase")]
    public LanguageText[] fase;
    

    [System.Serializable]
    public class Texto{        
        public LanguageText[] linguas;
    }
}
