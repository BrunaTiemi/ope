﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGameDifficulty : MonoBehaviour
{
    public GameDifficultyScriptableObject difficultySO;
    public void SetEasy(){
        difficultySO.SetEasy();
        Debug.Log("Nova dificuldade: " + difficultySO.dificuldadeAtual.ToString());
    }

    public void SetNormal(){
        difficultySO.SetNormal();
        Debug.Log("Nova dificuldade: " + difficultySO.dificuldadeAtual.ToString());
    }

    public void SetHard(){
        difficultySO.SetHard();
        Debug.Log("Nova dificuldade: " + difficultySO.dificuldadeAtual.ToString());
    }
}