﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Difficulty Values", menuName = "OPE/Difficulty")]
public class GameDifficultyScriptableObject : ScriptableObject
{
    public int maxLife;
    public float invincibleTime;
    public float shieldTime;

    //Enum
    public difEnum dificuldadeAtual;

    [System.Serializable] public enum difEnum {easy, normal, hard};

    //Array
    public difs[] dificuldades;

    public const int originalMaxLife = 20;

    public const float originalInvincibleTime = 0.5f, originalShieldTime = 5;

    //Lambdas
    string currentDif => PlayerPrefs.GetString("gameDifficulty", "normal");

    public void LoadDifficulty(){
        //pega a dificuldade salva no playerprefs
        dificuldadeAtual = (difEnum)System.Enum.Parse(typeof(difEnum), PlayerPrefs.GetString("gameDifficulty", "normal"));

        //passa os valores necessarios pro scriptable object
        for(int i = 0; i < dificuldades.Length; i++){
            if(dificuldadeAtual == dificuldades[i].dif){
                maxLife = dificuldades[i].maxLife;
                invincibleTime = dificuldades[i].invincibleTime;
                shieldTime = dificuldades[i].shieldTime;
                break;
            }
        }

    }

    #region Setting Difficulties

    public void SetEasy(){
        PlayerPrefs.SetString("gameDifficulty", difEnum.easy.ToString());
        LoadDifficulty();
    }

    public void SetNormal(){
        PlayerPrefs.SetString("gameDifficulty", difEnum.normal.ToString());
        LoadDifficulty();
    }

    public void SetHard(){
        PlayerPrefs.SetString("gameDifficulty", difEnum.hard.ToString());
        LoadDifficulty();
    }

    #endregion

    [System.Serializable]
    public class difs{
        public string difName;
        public int maxLife;
        public float invincibleTime, shieldTime;

        public difEnum dif;
    }
}
