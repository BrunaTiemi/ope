﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Flower Sprites", menuName = "OPE/Flor")]
public class SpritesFlorInfo : ScriptableObject
{
    public Sprite[] spritesFlor;
}
