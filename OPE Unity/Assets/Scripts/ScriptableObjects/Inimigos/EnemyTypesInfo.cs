﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "OPE/Inimigo")]
public class EnemyTypesInfo : ScriptableObject
{
    public int vida;
    public int dano;
    public float moveSpeed;
    public tipoAtq type;
    public GameObject shot;
}
[System.Serializable]
public enum tipoAtq{ground, air, aquatic, shooty, strong}

