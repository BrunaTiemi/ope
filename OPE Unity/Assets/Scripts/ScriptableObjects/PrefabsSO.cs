﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PrefabScriptableObjects", menuName = "OPE/PrefabsScripObj")] 
public class PrefabsSO : ScriptableObject
{
    public GameObject[] prefabs;
    public nivelDiferentePrefab[] prefabsPorNivel;

}

    [System.Serializable]
    public class nivelDiferentePrefab{
        public string nivel;
        public GameObject[] nivelPrefabs;
    }
