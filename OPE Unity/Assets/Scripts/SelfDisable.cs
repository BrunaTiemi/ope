﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDisable : MonoBehaviour
{
    public float delay;
    
    public void DisableWithDelay(){
        Invoke("DisableNow", delay);
    }

    public void DisableNow(){
        gameObject.SetActive(false);
    }
}
