﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalaDeLuta : MonoBehaviour
{   
    [Header("Recompensa. Tooltip")][Tooltip("Deve ser um objeto instanciado que vai ser ativado ao acabar as waves")]
    public GameObject reward;

    [Space]
    [SerializeField] private int currentWave;
    public bool activated, done;
    public MoveWhenShotTile[] portas;
    public Transform[] enemySpawnTransform;
    [SerializeField] private bool[] transformTaken; //pra nao spawnarem no mesmo transform

    [Tooltip("Quantos inimigos vão aparecer em cada wave")]
    public int[] enemiesPerWave;
    [Tooltip("Quantas vidas spawnar depois de matar uma wave")]
    public int lifeSpawnQuantity = 1;

    //prefabs
    [Header("Prefabs")]
    public PrefabsSO SOenemyPrefabs; //para instanciar inimigos
    public PrefabsSO itemsPrefab; //para instanciar o item de vida
    private nivelDiferentePrefab enemyPrefabs => SOenemyPrefabs.prefabsPorNivel[GameManager.nivelAtual-1];
    private GameObject healthItem => itemsPrefab.prefabs[0]; 

    [SerializeField] private List<GameObject> currentEnemies;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            if(activated == false && done == false){
                activated = true;
                StartFight();
            }
        }    
    }

    void StartFight(){
        for(int i = 0; i < portas.Length; i++){
            portas[i].Close();
        }
        transformTaken = new bool[enemySpawnTransform.Length];
        

        currentWave = 0;

        Invoke("SpawnEnemies", 1f);

        //eventos
        SendEventEvent.RemoveFromList += KilledEnemy;
        PlayerScript.PlayerRespawn += Restart;
    }

    private void OnDisable() {
        SendEventEvent.RemoveFromList -= KilledEnemy;
        PlayerScript.PlayerRespawn -= Restart;
    }

    void SpawnEnemies(){
        for(int i = 0; i < enemiesPerWave[currentWave]; i++){
            GameObject enemyToSpawn = enemyPrefabs.nivelPrefabs[Random.Range(0, enemyPrefabs.nivelPrefabs.Length)];

            int transformNum = Random.Range(0, enemySpawnTransform.Length);
            
            if (transformTaken[transformNum] == true){
                for( int j = 0; j < transformTaken.Length; j++){
                    transformNum = j;
                    if(transformTaken[transformNum] == false){
                        break;
                    }
                }
            }

            //instancia o inimigo e seta transformTaken no numero do transform q tirou
            GameObject enemy = Instantiate(enemyToSpawn, enemySpawnTransform[transformNum].position, Quaternion.identity);
            enemy.AddComponent<SendEventEvent>();
            currentEnemies.Add(enemy);
            transformTaken[transformNum] = true;
        }

        //depois de spawnar
        for( int j = 0; j < transformTaken.Length; j++){
            transformTaken[j] = false;
        }
    }

    void KilledEnemy(GameObject obj){
        if(activated) {
            //tira o inimigo morto da lista
            currentEnemies.Remove(obj);

            if(currentEnemies.Count < 1){
                for (int i = 0; i < lifeSpawnQuantity; i++)
                {                    
                    SpawnHealth();
                }
                Invoke("NextWave", 2f);
            }
        }
    }

    void SpawnHealth(){
        //pega o transform mais perto do meio pra spawnar a vida
        int middleTransform = enemySpawnTransform.Length / 2;
        Vector3 spawnHealthPos = enemySpawnTransform[middleTransform].position;

       //Instantiate
       Instantiate(healthItem, spawnHealthPos, Quaternion.identity);
    }

    void NextWave(){
        if(activated){
            currentWave ++;

            //se ainda tem waves, continua
            if(currentWave < enemiesPerWave.Length){
                SpawnEnemies();
            }
            else Finished();
        }
    }

    void Finished(){
        done = true;

        AbrirPortas();

        reward.SetActive(true);
    }

    //se o player morrer no meio
    void Restart(){
        CancelInvoke();
        currentWave = 0;
        activated = false;
        for(int i = currentEnemies.Count-1; i >= 0; i--){
            print(i);
            Destroy(currentEnemies[i]);
            currentEnemies.RemoveAt(i);
        }

        SendEventEvent.RemoveFromList -= KilledEnemy;
        PlayerScript.PlayerRespawn -= Restart;
        AbrirPortas();
    }
    void AbrirPortas(){
        for(int i = 0; i < portas.Length; i++){
            portas[i].Open();
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Player"){
            if(done){
                SendEventEvent.RemoveFromList -= KilledEnemy;
                PlayerScript.PlayerRespawn -= Restart;
            }
        }
    }

}
