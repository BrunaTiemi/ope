﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SendEventEvent : MonoBehaviour
{
    public static event Action<GameObject> RemoveFromList;
    private void OnDestroy() {
        if(RemoveFromList != null){
            RemoveFromList(this.gameObject);
        }
    }
}
