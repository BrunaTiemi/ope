﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndStage : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "Player") {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            gameManager.EndStage();
        }
    }
}
