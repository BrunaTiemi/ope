﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSelfDestruct : MonoBehaviour
{
    [Tooltip("How long until it self destructs")]
    public float seconds;
    void Start()
    {
        Invoke("SelfDestruct", seconds);
    }

    void SelfDestruct(){
        Destroy(this.gameObject);
    }
}
