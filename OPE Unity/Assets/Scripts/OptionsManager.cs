﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public static event Action UpdateOptionsEvent;
    public static bool optionsLoaded;

    public static float musicVolume;

    [Header("Objetos")]
    //objetos
    public Slider BgmVolumeSlider; //backgroundMusic

    // 0 = false, 1 = true;

    void Start()
    {
        LoadOptions();

        PrintOptions();
        UpdateVisuals();
    }

    public static void LoadOptions(){
        musicVolume = PlayerPrefs.GetFloat("BGVolume", 1f);

        optionsLoaded = true;
    }

    public static void OptionsLoaded(){
        if(!optionsLoaded){
            LoadOptions();
        }
    }

    void PrintOptions(){
    }

    public void UpdateOption(string optionName, int newValue){

        switch(optionName){
            default: Debug.Log("Option name not found! Did you type it correctly? If so, add it to the OptionsManager.");
                break;
        }

        PlayerPrefs.SetInt(optionName, newValue);
        print("Updated " + optionName + " " + newValue);

        if(UpdateOptionsEvent != null) UpdateOptionsEvent();
    }

    void UpdateVisuals(){
        if(BgmVolumeSlider != null){
            BgmVolumeSlider.value = musicVolume;
        }
    }

    
}
