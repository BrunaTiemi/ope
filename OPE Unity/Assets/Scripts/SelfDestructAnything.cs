﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructAnything : MonoBehaviour
{
    [Tooltip("Escreva todas as tags nessa linha")]
    [TextArea]
    public string ignoreTags;
    
    void Start(){
        ignoreTags += "Checkpoint ";
        ignoreTags += "SpawnArea ";
        ignoreTags += "DirectionCollider ";
    }

    private void OnTriggerEnter2D(Collider2D other) {
        
        bool isIgnored = ignoreTags.Contains(other.gameObject.tag);
        
        if(!isIgnored){
            Destroy(this.gameObject);
        }
    }

}
