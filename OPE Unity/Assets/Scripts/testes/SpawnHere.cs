﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHere : MonoBehaviour
{
    [Header("Aperte P para spawnar")]
    public GameObject prefab;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P)){
            Instantiate(prefab, transform);
        }
    }
}
