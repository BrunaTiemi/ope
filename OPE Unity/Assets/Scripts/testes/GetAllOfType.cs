﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GetAllOfType : MonoBehaviour
{
    public bool Inimigos, Itens, checkpoints;
    [Space]
    public bool reset;
    
    public List<Transform> T = new List<Transform>();

    private void Update() {
        if(Inimigos == true){
            Inimigos = false;
            GetAllEnemies();
        }
        if(Itens){
            Itens = false;
            GetAllItems();
        }
        if(checkpoints){
            checkpoints = false;
            GetAllCheckpoints();
        }



        if(reset){
            reset = false;
            T.Clear();
        }
    }

    void GetAllEnemies(){
        placeHolderScript[] ph = GameObject.FindObjectsOfType<placeHolderScript>();

        for (int i = 0; i < ph.Length; i++)
        {
            T.Add(ph[i].transform);
        }

        SendAllHere();
    }

    void GetAllCheckpoints(){
        Checkpoint[] c = GameObject.FindObjectsOfType<Checkpoint>();

        for (int i = 0; i < c.Length; i++)
        {
            T.Add(c[i].transform);
        }

        SendAllHere();
    }

    void GetAllItems(){
        GameObject[] k = GameObject.FindGameObjectsWithTag("Key");
        GameObject[] c = GameObject.FindGameObjectsWithTag("Carta");
        GameObject[] s = GameObject.FindGameObjectsWithTag("ItemShield");
        GameObject[] v = GameObject.FindGameObjectsWithTag("ItemVida");

        foreach (var item in k)
        {
            T.Add(item.transform);
        }
        foreach (var item in c)
        {
            T.Add(item.transform);
        }
        foreach (var item in s)
        {
            T.Add(item.transform);
        }
        foreach (var item in v)
        {
            T.Add(item.transform);
        }

        
        SendAllHere();
    }

    void SendAllHere(){
        for (int i = 0; i < T.Count; i++)
        {
            T[i].transform.SetParent(gameObject.transform);
        }
    }
}
